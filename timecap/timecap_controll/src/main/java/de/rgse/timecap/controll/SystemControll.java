package de.rgse.timecap.controll;

import org.apache.log4j.Logger;

import de.rgse.timecap.controll.util.PausenLauncher;
import de.rgse.timecap.service.ServiceProvider;
import de.rgse.timecap.service.TagAnalyseService;
import de.rgse.timecap.service.TagBeginnenService;

public class SystemControll {

    private final static Logger LOGGER = Logger.getLogger(SystemControll.class);

    public TagBeginnenService getTagBeginnenService() {
        return ServiceProvider.getTagBeginnenService();
    }

    public TagAnalyseService getTagAnalyseService() {
        return ServiceProvider.getTagAnalyseServiceInstance();
    }

    public void launchPausenThread() {
        PausenLauncher.schedulePause();
        SystemControll.LOGGER.info("Pausenthread initializied");
    }
}
