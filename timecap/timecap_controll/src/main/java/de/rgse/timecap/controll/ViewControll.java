package de.rgse.timecap.controll;

import javax.swing.JOptionPane;
import javax.swing.UIManager;

import org.apache.log4j.Logger;

import de.rgse.timecap.controll.util.Entscheidung;
import de.rgse.timecap.model.ReportObjekt;
import de.rgse.timecap.view.TrayMenu;
import de.rgse.timecap.view.pages.ErrorPage;
import de.rgse.timecap.view.pages.PotentielleKorrekturPage;
import de.rgse.timecap.view.util.IconFactory;
import de.rgse.timecap.view.util.IconGroesseEnum;

public class ViewControll {

    private TrayMenu trayMenu;

    private final static Logger LOGGER = Logger.getLogger(ViewControll.class);

    public ViewControll() {
        try {
            UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
            ViewControll.LOGGER.info("Look & Feel set");
        } catch (Exception e) {
            ViewControll.LOGGER.error(e);
        }
    }

    public TrayMenu getTrayMenu() {
        if (null == trayMenu) {
            trayMenu = new TrayMenu();
        }
        return trayMenu;
    }

    public Entscheidung confirmPauseEintragen() {

        int i = JOptionPane.showConfirmDialog(null, "Möchten Sie jetzt eine Pause eintragen?", "Pause",
                JOptionPane.YES_NO_CANCEL_OPTION, JOptionPane.QUESTION_MESSAGE, IconFactory.getLogoIconSmall());

        Entscheidung result;
        if (JOptionPane.YES_OPTION == i) {
            result = Entscheidung.JA;
        } else if (JOptionPane.NO_OPTION == i) {
            result = Entscheidung.NEIN;
        } else {
            result = Entscheidung.SPAETER;
        }
        return result;

    }

    public void arbeitstagExistiertPage() {
        int confirmation = JOptionPane.showConfirmDialog(null,
                "Es existiert bereits ein Tag mit dem heutigen Datum.\nSoll der Tag überschrieben werden?",
                "Tag existiert bereits", JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE,
                IconFactory.getQuestion(IconGroesseEnum.PX32));

        if (confirmation == JOptionPane.YES_OPTION) {
            try {
                ControllProvider.getSystemControllInstance().getTagBeginnenService().tagUeberschreiben();
            } catch (Exception e) {
                ViewControll.LOGGER.error(e);
                errorPage(e);
            }
        }
    }

    public void errorPage(Throwable e) {
        new ErrorPage(e);
    }

    public void potentielleKorrekturPage(ReportObjekt reportObjekt) {
        new PotentielleKorrekturPage(reportObjekt);
    }
}
