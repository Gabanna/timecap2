package de.rgse.timecap.controll.util;

import java.text.ParseException;
import java.util.GregorianCalendar;
import java.util.Locale;
import java.util.concurrent.Callable;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import org.apache.log4j.Logger;

import de.rgse.timecap.controll.ControllProvider;
import de.rgse.timecap.controll.SystemControll;
import de.rgse.timecap.controll.ViewControll;
import de.rgse.timecap.exceptions.PausenlimitErreichtException;
import de.rgse.timecap.service.PausenService;
import de.rgse.timecap.service.ServiceProvider;
import de.rgse.timecap.view.pages.PauseBeendenPage;

public class PauseCall implements Callable<Void> {

    private static final Logger LOGGER = Logger.getLogger(PauseCall.class);

    private Entscheidung entscheidung;

    private final ScheduledExecutorService executorService;

    private final PausenService pausenService = ServiceProvider.getPausenServiceInstance();

    private final ViewControll viewControll = ControllProvider.getViewControllInstance();

    private final SystemControll systemControll = ControllProvider.getSystemControllInstance();

    private final int DELAY_MINUTES = 30;

    public PauseCall(ScheduledExecutorService executorService) {
        this.executorService = executorService;
    }

    @Override
    public Void call() throws Exception {
        entscheidung = Entscheidung.SPAETER;
        while (warteMitPauseeintrag()) {
            executorService.schedule(new PauseCall(executorService), DELAY_MINUTES, TimeUnit.MINUTES);
        }

        if (entscheidung == Entscheidung.JA) {
            pauseEintragen();
        }
        return null;
    }

    private void pauseEintragen() {
        try {
            pausenService.pauseEintragen();
            new PauseBeendenPage();
        } catch (PausenlimitErreichtException e) {
            PauseCall.LOGGER.error(e);
        } catch (NumberFormatException e) {
            PauseCall.LOGGER.error(e);
        } catch (ParseException e) {
            PauseCall.LOGGER.error(e);
        }
    }

    private boolean warteMitPauseeintrag() {
        return (null == systemControll.getTagAnalyseService().findBy(new GregorianCalendar(Locale.GERMANY)).getPause() & (entscheidung = viewControll
                .confirmPauseEintragen()) == Entscheidung.SPAETER);
    }

}