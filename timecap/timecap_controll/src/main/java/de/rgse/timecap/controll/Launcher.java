package de.rgse.timecap.controll;

import de.rgse.timecap.exceptions.ArbeitstagExistiertException;
import de.rgse.timecap.exceptions.PotetielleKorrekturException;
import de.rgse.timecap.view.pages.SplashScreen;

public class Launcher {

    public static void main(String[] args) {
        SplashScreen splashScreen = new SplashScreen();
        ViewControll viewControll = ControllProvider.getViewControllInstance();
        try {
            SystemControll systemControll = ControllProvider.getSystemControllInstance();
            systemControll.launchPausenThread();

            try {
                systemControll.getTagBeginnenService().beginneTag();
            } catch (ArbeitstagExistiertException e) {
                viewControll.arbeitstagExistiertPage();
            }

            try {
                systemControll.getTagBeginnenService().letztenTagAufUeberlaengeTesten();
            } catch (PotetielleKorrekturException e) {
                viewControll.potentielleKorrekturPage(e.getReportObjekt());
            }
            viewControll.getTrayMenu();
        } catch (Throwable e) {
            viewControll.errorPage(e);
            splashScreen.dispose();
        }
        splashScreen.dispose();

    }
}
