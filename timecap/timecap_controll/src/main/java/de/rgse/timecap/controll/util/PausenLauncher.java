package de.rgse.timecap.controll.util;

import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

public abstract class PausenLauncher {

    private static final int hours = 4;

    private static final ScheduledExecutorService executorService = Executors.newScheduledThreadPool(1);

    public static void schedulePause() {
        PausenLauncher.executorService.schedule(new PauseCall(PausenLauncher.executorService), PausenLauncher.hours,
                TimeUnit.HOURS);
    }
}