package de.rgse.timecap.controll;

public class ControllProvider {

    private static SystemControll systemControll;

    private static ViewControll viewControll;

    public static SystemControll getSystemControllInstance() {
        if (null == ControllProvider.systemControll) {
            ControllProvider.systemControll = new SystemControll();
        }
        return ControllProvider.systemControll;
    }

    public static ViewControll getViewControllInstance() {
        if (null == ControllProvider.viewControll) {
            ControllProvider.viewControll = new ViewControll();
        }
        return ControllProvider.viewControll;
    }
}
