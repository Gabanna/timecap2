package de.rgse.timecap.persistence;

import de.rgse.timecap.model.ArbeitstagFactory;

public class PersistenceProvider {

    private static ArbeitstagDAO arbeitstagDAO;

    private static ReportObjektDAO reportObjektDAO;

    private static ArbeitstagFactory arbeitstagFactory;

    public static ArbeitstagDAO getArbeitstagDAOInstance() {
        if (null == PersistenceProvider.arbeitstagDAO) {
            PersistenceProvider.arbeitstagDAO = new ArbeitstagDAO();
        }
        return PersistenceProvider.arbeitstagDAO;
    }

    public static ReportObjektDAO getReportObjektDAOInstance() {
        if (null == PersistenceProvider.reportObjektDAO) {
            PersistenceProvider.reportObjektDAO = new ReportObjektDAO();
        }
        return PersistenceProvider.reportObjektDAO;
    }

    public static ArbeitstagFactory getArbeitstagFactoryInstance() {
        if (null == PersistenceProvider.arbeitstagFactory) {
            PersistenceProvider.arbeitstagFactory = new ArbeitstagFactory();
        }
        return PersistenceProvider.arbeitstagFactory;
    }
}
