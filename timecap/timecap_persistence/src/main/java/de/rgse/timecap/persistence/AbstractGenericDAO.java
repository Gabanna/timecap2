package de.rgse.timecap.persistence;

import java.io.Serializable;
import java.util.List;

import javax.persistence.EntityExistsException;
import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;

import org.apache.log4j.Logger;

/**
 * @author Gabanna
 * @version $Revision: 1.0 $
 */
public abstract class AbstractGenericDAO<T> implements Serializable {

    private static final long serialVersionUID = 4072457267450465757L;

    private EntityManager entityManager;

    private static final Logger LOGGER = Logger.getLogger(AbstractGenericDAO.class);

    /**
     * Method delete.
     * 
     * @param delete
     *            T
     * 
     * 
     * @return boolean
     * @throws Exception
     */
    public void delete(T delete) throws Exception {
        AbstractGenericDAO.LOGGER.info("deleting " + delete);
        this.entityManager = DBUtil.getEntityManager();
        EntityTransaction transaction = entityManager.getTransaction();
        try {
            transaction.begin();
            this.entityManager.remove(delete);
            transaction.commit();
            AbstractGenericDAO.LOGGER.info("deleting done");
        } catch (Exception e) {
            transaction.rollback();
            AbstractGenericDAO.LOGGER.error(e);
            throw e;
        }
    }

    /**
     * Method findById.
     * 
     * @param id
     *            long
     * 
     * 
     * @return T
     */
    public abstract T findById(long id);

    /**
     * Method findAll.
     * 
     * 
     * 
     * @return List<T>
     */
    public abstract List<T> getAll();

    /**
     * Method merge.
     * 
     * @param merge
     *            T
     * 
     * 
     * @return T
     */
    public T merge(T merge) {
        AbstractGenericDAO.LOGGER.info("merging " + merge);
        this.entityManager = DBUtil.getEntityManager();
        EntityTransaction transaction = entityManager.getTransaction();
        try {
            transaction.begin();
            merge = this.entityManager.merge(merge);
            transaction.commit();
            AbstractGenericDAO.LOGGER.info("merging done");
        } catch (IllegalArgumentException e) {
            transaction.rollback();
            AbstractGenericDAO.LOGGER.error(e);
            throw e;
        }
        return merge;
    }

    /**
     * Method persist.
     * 
     * @param persist
     *            T
     * 
     * 
     * @return boolean
     */
    public void persist(T persist) {
        AbstractGenericDAO.LOGGER.info("persisting" + persist);
        this.entityManager = DBUtil.getEntityManager();
        EntityTransaction transaction = entityManager.getTransaction();
        try {
            transaction.begin();
            this.entityManager.persist(persist);
            transaction.commit();
            AbstractGenericDAO.LOGGER.info("persisting done");
        } catch (EntityExistsException e) {
            transaction.rollback();
            AbstractGenericDAO.LOGGER.error(e);
            throw e;
        } catch (IllegalArgumentException e) {
            transaction.rollback();
            AbstractGenericDAO.LOGGER.error(e);
            throw e;
        } catch (IllegalStateException e) {
            transaction.rollback();
            AbstractGenericDAO.LOGGER.error(e);
            throw e;
        }
    }
}
