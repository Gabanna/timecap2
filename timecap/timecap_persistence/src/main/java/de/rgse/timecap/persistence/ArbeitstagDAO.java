package de.rgse.timecap.persistence;

import java.util.Calendar;
import java.util.List;

import javax.persistence.Query;

import org.apache.log4j.Logger;

import com.mysema.query.jpa.impl.JPAQuery;

import de.rgse.timecap.model.Arbeitstag;
import de.rgse.timecap.model.QArbeitstag;
import de.rgse.timecap.model.util.Datum;
import de.rgse.timecap.model.util.QDatum;

public class ArbeitstagDAO extends AbstractGenericDAO<Arbeitstag> {

    private static final Logger LOGGER = Logger.getLogger(ArbeitstagDAO.class);

	private static final long serialVersionUID = 3174049035993392182L;

	private final QArbeitstag qArbeitstag = QArbeitstag.arbeitstag;

	ArbeitstagDAO() {
	}

	@Override
	public Arbeitstag findById(long id) {
		return DBUtil.getEntityManager().find(Arbeitstag.class, id);
	}

	public Arbeitstag findByDatum(Calendar calendar) {
		Calendar date = UhrzeitUtil.formatToDate(calendar);
		return new JPAQuery(DBUtil.getEntityManager()).from(qArbeitstag)
				.where(qArbeitstag.datum.eq(date)).singleResult(qArbeitstag);
        } catch (Throwable e) {
            ArbeitstagDAO.LOGGER.info("Standartabfrage wirft Fehler: " + e.getMessage());
            ArbeitstagDAO.LOGGER.info("Benutzer Kompatibilitätsabfrage");
            int day = calendar.get(Calendar.DAY_OF_MONTH);
            int month = calendar.get((Calendar.MONTH)) + 1;
            int year = calendar.get(Calendar.YEAR);
            Query query = DBUtil.getEntityManager().createNativeQuery("select * from arbeitstag where datum=:datum");
            query.setParameter("datum", year + "-" + month + "-" + day + "%");
            result = (Arbeitstag) query.getSingleResult();
	}

        return result;
    }

    public Arbeitstag findByDatum(int tag, int monat, int jahr) {
        QDatum datum = qArbeitstag.datum();
        return new JPAQuery(DBUtil.getEntityManager()).from(qArbeitstag)
                .where(datum.tag.eq(tag).and(datum.monat.eq(monat).and(datum.jahr.eq(jahr)))).singleResult(qArbeitstag);
    }

	@Override
	public List<Arbeitstag> getAll() {
		return new JPAQuery(DBUtil.getEntityManager()).from(qArbeitstag).list(
				qArbeitstag);
	}

	public List<Arbeitstag> findAllByMonth(Calendar calendar) {
		GregorianCalendar from = (GregorianCalendar) calendar.clone();
		from.set(Calendar.DAY_OF_MONTH, 1);

		GregorianCalendar to = (GregorianCalendar) calendar.clone();
		to.set(Calendar.DAY_OF_MONTH,
				calendar.getActualMaximum(Calendar.DAY_OF_MONTH));

		return new JPAQuery(DBUtil.getEntityManager()).from(qArbeitstag)
				.where(qArbeitstag.datum.between(from, to)).list(qArbeitstag);
	}

	public List<Arbeitstag> findAllByYear(Calendar calendar) {
		GregorianCalendar from = (GregorianCalendar) calendar.clone();
		from.set(Calendar.DAY_OF_MONTH, 1);
		from.set(Calendar.MONTH, 0);

		GregorianCalendar to = (GregorianCalendar) calendar.clone();
		to.set(Calendar.MONTH, calendar.getActualMaximum(Calendar.MONTH));
		to.set(Calendar.DAY_OF_MONTH,
				calendar.getActualMaximum(Calendar.DAY_OF_MONTH));

		return new JPAQuery(DBUtil.getEntityManager()).from(qArbeitstag)
				.where(qArbeitstag.datum.between(from, to)).list(qArbeitstag);
	}
}
