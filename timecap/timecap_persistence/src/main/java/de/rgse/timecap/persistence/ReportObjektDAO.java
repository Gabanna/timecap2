package de.rgse.timecap.persistence;

import java.util.Calendar;
import java.util.List;

import com.mysema.query.jpa.impl.JPAQuery;

import de.rgse.timecap.model.Arbeitstag;
import de.rgse.timecap.model.QReportObjekt;
import de.rgse.timecap.model.ReportObjekt;
import de.rgse.timecap.model.util.Datum;
import de.rgse.timecap.model.util.QDatum;

public class ReportObjektDAO extends AbstractGenericDAO<ReportObjekt> {

    private static final long serialVersionUID = -5716022980199742134L;

    private final QReportObjekt qReportObjekt = QReportObjekt.reportObjekt;

    ReportObjektDAO() {
    }

    @Override
    public ReportObjekt findById(long id) {
        return DBUtil.getEntityManager().find(ReportObjekt.class, id);
    }

    public ReportObjekt findByArbeitstag(Arbeitstag arbeitstag) {
        return new JPAQuery(DBUtil.getEntityManager()).from(qReportObjekt)
                .where(qReportObjekt.arbeitstag().eq(arbeitstag)).singleResult(qReportObjekt);
    }

    public ReportObjekt findByDatum(Calendar datum) {
        Datum date = new Datum(datum);
        QDatum qdatum = qReportObjekt.arbeitstag().datum();
        return new JPAQuery(DBUtil.getEntityManager())
                .from(qReportObjekt)
                .where(qdatum.tag.eq(date.getTag()).and(
                        qdatum.monat.eq(date.getMonat()).and(qdatum.jahr.eq(date.getJahr()))))
                .singleResult(qReportObjekt);
    }

    @Override
    public List<ReportObjekt> getAll() {
        return new JPAQuery(DBUtil.getEntityManager()).from(qReportObjekt).list(qReportObjekt);
    }
}
