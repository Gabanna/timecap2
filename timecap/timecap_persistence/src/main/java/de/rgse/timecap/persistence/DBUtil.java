package de.rgse.timecap.persistence;

import javax.persistence.EntityManager;
import javax.persistence.Persistence;

import org.apache.log4j.Logger;

/**
 * @author Gabanna
 * @version $Revision: 1.0 $
 */
public class DBUtil {

    private static final Logger LOGGER = Logger.getLogger(DBUtil.class);

    private static EntityManager ENTITY_MANAGER;

    static {
        try {
            DBUtil.ENTITY_MANAGER = Persistence.createEntityManagerFactory("produktivPU").createEntityManager();
        } catch (Throwable e) {
            DBUtil.LOGGER.error("Persistenceexception: " + e);
        }
    }

    public static EntityManager getEntityManager() {
        if (null == DBUtil.ENTITY_MANAGER || !DBUtil.ENTITY_MANAGER.isOpen()) {
            DBUtil.ENTITY_MANAGER = Persistence.createEntityManagerFactory("produktivPU").createEntityManager();
        }
        return DBUtil.ENTITY_MANAGER;
    }
}
