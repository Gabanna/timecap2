package de.rgse.timecap.view.data;

import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Locale;

import de.rgse.timecap.model.ReportObjekt;
import de.rgse.timecap.service.ServiceProvider;

public class VertretungsplanerData {

    private ReportObjekt reportObjekt;

    private Calendar vonCalendar;

    private Calendar bisCalendar;

    public VertretungsplanerData(ReportObjekt reportObjekt) {
        this.reportObjekt = reportObjekt;
        vonCalendar = reportObjekt.getArbeitstag().getDatum().asCalendar();
        vonCalendar.set(Calendar.DATE, vonCalendar.get(Calendar.DATE) + 1);
        bisCalendar = (Calendar) vonCalendar.clone();
        bisCalendar.set(Calendar.DATE, bisCalendar.get(Calendar.DATE) + 1);
    }

    public ReportObjekt getReportObjekt() {
        return reportObjekt;
    }

    public Calendar getVonCalendar() {
        return vonCalendar;
    }

    public Calendar getBisCalendar() {
        return bisCalendar;
    }

    public void okAction(Date vonDatum, Date bisDatum) {
        GregorianCalendar calendar = new GregorianCalendar(Locale.GERMANY);
        calendar.setTime(bisDatum);
        int bisDate = calendar.get(Calendar.DATE);
        calendar.setTime(vonDatum);
        int vonDate = calendar.get(Calendar.DATE);
        for (int vonDate2 = vonDate; vonDate2 <= bisDate; vonDate2++) {
            calendar.set(Calendar.DATE, vonDate2);
            ServiceProvider.getTagBeginnenService().erstelleVertretungstag(calendar);
        }
    }
}
