package de.rgse.timecap.view.data;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JMenuItem;
import javax.swing.JOptionPane;

import org.apache.log4j.Logger;

import de.rgse.timecap.exceptions.ArbeitszeitUeberschreitungsException;
import de.rgse.timecap.exceptions.PausenlimitErreichtException;
import de.rgse.timecap.service.PausenService;
import de.rgse.timecap.service.ServiceProvider;
import de.rgse.timecap.service.TagBeendenService;
import de.rgse.timecap.view.TrayMenu;
import de.rgse.timecap.view.pages.InfoPage;
import de.rgse.timecap.view.pages.PauseBeendenPage;
import de.rgse.timecap.view.pages.UebersichtArbeitstagePage;
import de.rgse.timecap.view.util.IconFactory;
import de.rgse.timecap.view.util.IconGroesseEnum;

public class TrayMenuData {

    private final static Logger LOGGER = Logger.getLogger(TrayMenu.class);

    private final PausenService pausenService = ServiceProvider.getPausenServiceInstance();

    private final TagBeendenService tagBeendenService = ServiceProvider.getTagBeendenServiceInstance();

    private List<JMenuItem> menuItems;

    public TrayMenuData() {
        menuItems = initMenuItems();
    }

    public List<JMenuItem> getMenuItems() {
        return menuItems;
    }

    private List<JMenuItem> initMenuItems() {
        menuItems = new ArrayList<JMenuItem>();

        JMenuItem exitOption = new JMenuItem("Beenden", IconFactory.getExitOption(IconGroesseEnum.PX16));
        exitOption.addActionListener(getExitOptionAction());

        JMenuItem uebersichtArbeitstage = new JMenuItem("Übersicht der Arbeitstage",
                IconFactory.getUeberblick(IconGroesseEnum.PX16));
        uebersichtArbeitstage.addActionListener(getUebersichtArbeitstageOptionAction());

        JMenuItem reportArbeitstage = new JMenuItem("Bericht erstellen", IconFactory.getExport(IconGroesseEnum.PX16));
        reportArbeitstage.addActionListener(getReportAction());

        JMenuItem infoOption = new JMenuItem("Info", IconFactory.getInfoOption(IconGroesseEnum.PX16));
        infoOption.addActionListener(getInfoAction());

        menuItems.add(uebersichtArbeitstage);
        menuItems.add(reportArbeitstage);
        menuItems.add(infoOption);
        menuItems.add(exitOption);
        return menuItems;
    }

    private ActionListener getInfoAction() {
        return new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                new InfoPage();
            }
        };
    }

    private ActionListener getReportAction() {
        return new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                new ReportSelectionData().reportAction();
            }
        };
    }

    public void getTrayAction() {
        int option = JOptionPane.showConfirmDialog(null, "Soll jetzt eine Pause eingetragen werden?", "Pause",
                JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE, IconFactory.getLogoIconSmall());
        if (option == JOptionPane.YES_OPTION) {
            try {
                pausenService.pauseEintragen();
            } catch (PausenlimitErreichtException e1) {
                TrayMenuData.LOGGER.error(e1);
            } catch (NumberFormatException e1) {
                TrayMenuData.LOGGER.error(e1);
            } catch (ParseException e1) {
                TrayMenuData.LOGGER.error(e1);
            }
            new PauseBeendenPage();
        }
    }

    private ActionListener getExitOptionAction() {
        return new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                if (JOptionPane.YES_OPTION == JOptionPane.showConfirmDialog(null, "TimeCap beenden?", "Beenden",
                        JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE,
                        IconFactory.getExitOption(IconGroesseEnum.PX32))) {
                    try {
                        tagBeendenService.tagBeenden();
                    } catch (ArbeitszeitUeberschreitungsException ex) {
                        JOptionPane.showMessageDialog(null, ex.getMessage());
                    }
                    TrayMenuData.LOGGER.info("_____END_____");
                    System.exit(0);
                }
            }
        };
    }

    private ActionListener getUebersichtArbeitstageOptionAction() {
        return new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                new UebersichtArbeitstagePage();
            }
        };
    }
}
