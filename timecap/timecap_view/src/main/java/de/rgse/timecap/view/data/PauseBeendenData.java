package de.rgse.timecap.view.data;

import java.util.GregorianCalendar;
import java.util.Locale;

import de.rgse.timecap.model.Arbeitstag;
import de.rgse.timecap.model.Pause;
import de.rgse.timecap.model.util.UhrzeitUtil;
import de.rgse.timecap.service.ServiceProvider;
import de.rgse.timecap.service.TagAnalyseService;
import de.rgse.timecap.view.exceptions.PausenendeExistsException;

public class PauseBeendenData {

    private TagAnalyseService tagAnalyseService = ServiceProvider.getTagAnalyseServiceInstance();

    public void pauseBeendenAction() throws PausenendeExistsException {
        GregorianCalendar calendar = new GregorianCalendar(Locale.GERMANY);
        Arbeitstag arbeitstag = tagAnalyseService.findBy(calendar);
        Pause pause = arbeitstag.getPause();

        if (null == pause.getEnde()) {
            pause.setEnde(UhrzeitUtil.format(calendar));

        } else {
            throw new PausenendeExistsException();
        }
        tagAnalyseService.update(arbeitstag);
    }

}
