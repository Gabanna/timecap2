package de.rgse.timecap.view.pages;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

import org.jdesktop.swingx.JXTitledPanel;

import de.rgse.timecap.view.FrameTemplate;
import de.rgse.timecap.view.data.PauseBeendenData;
import de.rgse.timecap.view.exceptions.PausenendeExistsException;
import de.rgse.timecap.view.util.IconFactory;

public class PauseBeendenPage extends FrameTemplate {

    private static final long serialVersionUID = 1L;

    private final PauseBeendenData pauseBeendenData;

    public PauseBeendenPage() {
        super("Pause", null);
        pauseBeendenData = new PauseBeendenData();

        JPanel panel = new JPanel(new BorderLayout());
        FrameTemplate.initComponent(panel);

        JLabel icon = new JLabel(IconFactory.getCoffee());
        FrameTemplate.initComponent(icon);

        panel.add(BorderLayout.CENTER, icon);
        panel.add(BorderLayout.SOUTH, createSouthPanel());

        JXTitledPanel contentPane = new JXTitledPanel(getTitle(), panel);
        FrameTemplate.initComponent(contentPane);

        contentPane.setBorder(BorderFactory.createEmptyBorder(20, 20, 20, 20));

        setContentPane(panel);
        pack();
        setAlwaysOnTop(true);
        setVisible(true);
    }

    private JPanel createSouthPanel() {
        JButton pauseBeendenBtn = new JButton("Die Pause beenden");
        FrameTemplate.initComponent(pauseBeendenBtn);
        pauseBeendenBtn.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    pauseBeendenData.pauseBeendenAction();
                } catch (PausenendeExistsException e1) {
                    new ErrorPage(e1);
                }
                dispose();
            }
        });

        JPanel panel = new JPanel();
        FrameTemplate.initComponent(panel);
        panel.add(pauseBeendenBtn);
        return panel;
    }

    public static void main(String[] args) {
        new PauseBeendenPage();
    }
}
