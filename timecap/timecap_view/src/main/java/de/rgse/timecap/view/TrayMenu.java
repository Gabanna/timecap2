package de.rgse.timecap.view;

import java.awt.AWTException;
import java.awt.SystemTray;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BorderFactory;
import javax.swing.JMenuItem;
import javax.swing.JPopupMenu;

import de.rgse.timecap.view.data.TrayMenuData;
import de.rgse.timecap.view.pages.ErrorPage;
import de.rgse.timecap.view.util.IconFactory;
import de.rgse.timecap.view.util.JTrayIcon;
import de.rgse.timecap.view.util.ViewUtil;

public class TrayMenu {

    private final TrayMenuData trayMenuData;

    public TrayMenu() {
        trayMenuData = new TrayMenuData();
        SystemTray tray = SystemTray.getSystemTray();
        JPopupMenu popup = new JPopupMenu("Zeiterfassung");

        for (JMenuItem item : trayMenuData.getMenuItems()) {
            popup.add(item);
        }

        popup.setBorder(BorderFactory.createLineBorder(ViewUtil.getGreencolor()));

        JTrayIcon trayIcon = new JTrayIcon(IconFactory.getLogoIconTray().getImage());
        trayIcon.setJPopupMenu(popup);
        trayIcon.setToolTip("Zeiterfassung");
        trayIcon.addActionListener(getTrayListener());
        try {
            tray.add(trayIcon);
        } catch (AWTException e) {
            new ErrorPage(e);
        }
    }

    private ActionListener getTrayListener() {
        return new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                trayMenuData.getTrayAction();
            }
        };
    }
}
