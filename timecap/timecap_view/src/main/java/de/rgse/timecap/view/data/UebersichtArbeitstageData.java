package de.rgse.timecap.view.data;

import java.io.Serializable;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Locale;

import de.rgse.timecap.model.Arbeitstag;
import de.rgse.timecap.service.ServiceProvider;
import de.rgse.timecap.service.TagAnalyseService;
import de.rgse.timecap.view.pages.DetailPage;
import de.rgse.timecap.view.pages.UebersichtArbeitstagePage;
import de.rgse.timecap.view.util.TableModel;

public class UebersichtArbeitstageData implements Serializable {

    private static final long serialVersionUID = 1L;

    private final Calendar calendar;

    private final TagAnalyseService tagAnalyseService = ServiceProvider.getTagAnalyseServiceInstance();

    private TableModel tableModel;

    public UebersichtArbeitstageData(UebersichtArbeitstagePage page) {
        calendar = new GregorianCalendar(Locale.GERMANY);
        initTableModel();
    }

    private void initTableModel() {
        tableModel = new TableModel(tagAnalyseService);

        String[] columnNames = { "ID", "Datum", "Tagesstart", "Tagesende", "Pausenbeginn", "Pausenende" };
        tableModel.setRowCount(columnNames.length);
        tableModel.setColumnIdentifiers(columnNames);
    }

    public void updateModel(Date date) {
        tableModel.update(date);
    }

    public void updateModel() {
        tableModel.update(calendar.getTime());
    }

    public TableModel getTableModel() {
        return tableModel;
    }

    public Calendar getSelectedMonth() {
        return calendar;
    }

    public void showDetails(long id) {
        new DetailPage(id);
    }

    public void showDetails() {
        new DetailPage();
    }

    public List<Arbeitstag> getDays() {
        return tagAnalyseService.findAllByMonth(getSelectedMonth());
    }

    public void removeDay(long id) throws Exception {
        tagAnalyseService.removeDay(id);
    }
}
