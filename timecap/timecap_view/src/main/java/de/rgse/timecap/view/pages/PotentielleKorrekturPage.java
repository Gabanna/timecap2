package de.rgse.timecap.view.pages;

import java.awt.BorderLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.JTextArea;

import org.jdesktop.swingx.JXTitledPanel;

import de.rgse.timecap.model.ReportObjekt;
import de.rgse.timecap.view.FrameTemplate;
import de.rgse.timecap.view.data.PotentielleKorrekturData;
import de.rgse.timecap.view.util.IconFactory;
import de.rgse.timecap.view.util.IconGroesseEnum;

public class PotentielleKorrekturPage extends FrameTemplate {

    private static final long serialVersionUID = 1L;

    private final PotentielleKorrekturData data;

    private final ReportObjekt reportObjekt;

    public PotentielleKorrekturPage(ReportObjekt reportObjekt) {
        super("Timecap", IconFactory.getQuestion(IconGroesseEnum.PX32));
        this.reportObjekt = reportObjekt;
        data = new PotentielleKorrekturData();

        JPanel panel = new JPanel(new BorderLayout());
        FrameTemplate.initComponent(panel);

        panel.add(BorderLayout.CENTER, createCenterPanel());
        panel.add(BorderLayout.SOUTH, createSouthPanel());

        JXTitledPanel contentPane = new JXTitledPanel(getTitle(), panel);
        FrameTemplate.initComponent(contentPane);

        contentPane.setBorder(BorderFactory.createEmptyBorder(20, 20, 20, 20));

        setContentPane(contentPane);
        pack();
        setVisible(true);
    }

    private JPanel createCenterPanel() {
        JPanel panel = new JPanel(new BorderLayout());
        FrameTemplate.initComponent(panel);

        JTextArea textArea = new JTextArea();
        textArea.setLineWrap(true);
        textArea.setEditable(false);
        textArea.setText("Der letzte Arbeitstageintrag ist von 09:00 bis 19:00 eingetragen.\nIst der Eintrag korrekt, oder soll er korrigiert werden?");
        FrameTemplate.initComponent(textArea);

        JButton jaBtn = new JButton("Der Eintrag ist korrekt", IconFactory.getOK(IconGroesseEnum.PX16));
        FrameTemplate.initComponent(jaBtn);
        jaBtn.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                new VertretungsplanerPage(reportObjekt);
                dispose();
            }
        });

        JButton neinBtn = new JButton("Der Eintrag soll auf 10:00 korrigiert werden",
                IconFactory.getPencil(IconGroesseEnum.PX16));
        FrameTemplate.initComponent(neinBtn);
        neinBtn.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                data.getBeginnKorrigierenAction(reportObjekt);
            }
        });

        JButton nein2Btn = new JButton("Der Eintrag soll auf 18:00 korrigiert werden",
                IconFactory.getPencil(IconGroesseEnum.PX16));
        FrameTemplate.initComponent(neinBtn);
        neinBtn.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                data.getEndeKorrigierenAction(reportObjekt);
            }
        });

        JPanel optionPanel = new JPanel(new GridLayout(1, 0));
        optionPanel.add(jaBtn);
        optionPanel.add(neinBtn);
        optionPanel.add(nein2Btn);

        panel.add(BorderLayout.CENTER, textArea);
        panel.add(BorderLayout.SOUTH, optionPanel);
        return panel;
    }

    private JPanel createSouthPanel() {
        return new JPanel();
    }
}
