package de.rgse.timecap.view.pages;

import java.awt.BorderLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.UIManager;

import org.apache.log4j.Logger;
import org.jdesktop.swingx.JXPanel;
import org.jdesktop.swingx.JXTaskPane;
import org.jdesktop.swingx.JXTaskPaneContainer;
import org.jdesktop.swingx.JXTitledPanel;

import de.rgse.timecap.view.FrameTemplate;
import de.rgse.timecap.view.data.ErrorData;
import de.rgse.timecap.view.util.IconFactory;
import de.rgse.timecap.view.util.IconGroesseEnum;
import de.rgse.timecap.view.util.ViewUtil;

public class ErrorPage extends FrameTemplate {

    private static final long serialVersionUID = -1394026516393446162L;

    private static final Logger LOGGER = Logger.getLogger(ErrorPage.class);

    private final ErrorData data;

    public ErrorPage(Throwable throwable) {
        super("Fehlermeldung", IconFactory.getBug(IconGroesseEnum.PX32));
        setLayout(new BorderLayout());
        ErrorPage.LOGGER.error("", throwable);

        data = new ErrorData(throwable);

        JXTaskPaneContainer panel = new JXTaskPaneContainer();
        FrameTemplate.initComponent(panel);

        panel.add(ceateTitle());
        panel.add(createTextArea());

        JXTitledPanel contentPane = new JXTitledPanel(getTitle(), panel);
        FrameTemplate.initComponent(contentPane);

        JXPanel content = new JXPanel(new BorderLayout());
        content.add(BorderLayout.CENTER, new JScrollPane(contentPane));
        content.add(BorderLayout.SOUTH, createControllPanel());

        content.setBorder(BorderFactory.createEmptyBorder(20, 20, 20, 20));

        setContentPane(content);
        setVisible(true);
    }

    private JPanel createControllPanel() {

        JButton btnSchliessen = new JButton("Schließen", IconFactory.getCancle(IconGroesseEnum.PX16));
        FrameTemplate.initComponent(btnSchliessen);
        btnSchliessen.addActionListener(schliessenAction());

        JButton btnZwischenablage = new JButton("in Zwischenablage legen", IconFactory.getCopy(IconGroesseEnum.PX16));
        FrameTemplate.initComponent(btnZwischenablage);
        btnZwischenablage.addActionListener(zwischenablageAction());

        JPanel panel = new JPanel(new GridLayout(1, 0));
        FrameTemplate.initComponent(panel);
        panel.add(btnZwischenablage);
        panel.add(btnSchliessen);
        return panel;
    }

    private ActionListener zwischenablageAction() {
        return new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                data.zwischenablageAction(e);
            }
        };
    }

    private ActionListener schliessenAction() {
        return new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                System.exit(-1);
            }
        };
    }

    private JLabel ceateTitle() {
        JLabel label = new JLabel("Es ist ein Fehler aufgetreten: " + data.getThrowable().getMessage());
        FrameTemplate.initComponent(label);

        return label;
    }

    private JXTaskPane createTextArea() {
        String stackTrace = data.getStacktrace();

        JTextArea textArea = new JTextArea(stackTrace);
        textArea.setEditable(false);
        FrameTemplate.initComponent(textArea);

        UIManager.put("TaskPane.titleBackgroundGradientStart", ViewUtil.getGreencolor());

        JXTaskPane taskPane = new JXTaskPane();
        taskPane.setBackground(ViewUtil.getGreencolor());
        taskPane.setCollapsed(true);
        taskPane.setTitle("Details");
        taskPane.add(new JScrollPane(textArea));

        return taskPane;
    }

    public static void main(String[] args) {
        new ErrorPage(new NullPointerException("Ha Ha!"));
    }
}
