package de.rgse.timecap.view.util;

import java.awt.Color;
import java.util.Calendar;
import java.util.GregorianCalendar;

import javax.swing.JFrame;
import javax.swing.JMenuBar;
import javax.swing.JSpinner;
import javax.swing.SpinnerDateModel;

import org.jdesktop.swingx.painter.RectanglePainter;

public class ViewUtil extends Color {

    private static final long serialVersionUID = 1L;

    private ViewUtil(int rgb) {
        super(rgb);
    }

    private final static Color GREENCOLOR = new Color(78, 184, 76);

    private final static Color GREYCOLOR = new Color(139, 139, 139);

    private static RectanglePainter rectanglePainter;

    public static Color getGreencolor() {
        return ViewUtil.GREENCOLOR;
    }

    public static Color getGreyColor() {
        return ViewUtil.GREYCOLOR;
    }

    /**
     * Method getDatePicker.
     * 
     * 
     * 
     * @return JSpinner
     */
    public static JSpinner createDatePicker(Calendar calendar) {
        JSpinner spinner = ViewUtil.getMonthPicker(calendar);
        JSpinner.DateEditor de = new JSpinner.DateEditor(spinner, "dd.MM.yyyy");
        spinner.setEditor(de);
        return spinner;
    }

    public static JSpinner getMonthPicker(Calendar calendar) {
        JSpinner spinner = new JSpinner();
        SpinnerDateModel sm = new SpinnerDateModel(calendar.getTime(), null, null, Calendar.MONTH);
        spinner.setModel(sm);
        JSpinner.DateEditor de = new JSpinner.DateEditor(spinner, "MM.yyyy");
        spinner.setEditor(de);
        return spinner;
    }

    public static JSpinner getDatePicker(GregorianCalendar calendar) {
        JSpinner spinner = new JSpinner();
        SpinnerDateModel sm = new SpinnerDateModel(calendar.getTime(), null, null, Calendar.DATE);
        spinner.setModel(sm);
        JSpinner.DateEditor de = new JSpinner.DateEditor(spinner, "dd.MM.yyyy");
        spinner.setEditor(de);
        return spinner;
    }

    /**
     * Method createMenuBar.
     * 
     * @param frame
     *            JFrame
     * @return JMenuBar
     */
    public static JMenuBar createMenuBar(JFrame frame) {
        JMenuBar bar = new JMenuBar();
        return bar;
    }

    public static RectanglePainter getTitlePainter() {
        if (null == ViewUtil.rectanglePainter) {
            ViewUtil.rectanglePainter = new RectanglePainter(ViewUtil.getGreencolor(), ViewUtil.getGreyColor());
        }
        return ViewUtil.rectanglePainter;
    }
}
