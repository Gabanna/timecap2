package de.rgse.timecap.view.data;

import java.util.MissingResourceException;
import java.util.ResourceBundle;

import org.apache.log4j.Logger;

public class InfoData {

    private final static Logger LOGGER = Logger.getLogger(InfoData.class);

    public final String user = System.getProperty("user.name");

    public final String osName = System.getProperty("os.name");

    public final String osArch = System.getProperty("os.arch");

    public final String osVersion = System.getProperty("os.version");

    public final String javaVersion = System.getProperty("java.version");

    public final String javaHome = System.getProperty("java.home");

    public final String javaClassPath = getMainPath();

    public final String buildVersion;

    public final String credits = "© 2013 - 2014 Ronny Gallin Software Engineering";

    public final String title = "Information";

    public InfoData() {
        buildVersion = getBuildVersion();
    }

    private String getMainPath() {
        String mainPath = InfoData.class.getResource("InfoData.class").getFile();
        return mainPath.replace("de/rgse/timecap/view/data/InfoData.class", "");
    }

    public String getBuildVersion() {
        String result = "UNDEFINED";
        ResourceBundle rb;
        try {
            rb = ResourceBundle.getBundle("main");
            result = rb.getString("application.version");
        } catch (MissingResourceException e) {
            InfoData.LOGGER
                    .warn("Resource bundle 'primefaces-extensions' was not found or error while reading current version.");
        }
        return result;
    }
}
