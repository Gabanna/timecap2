package de.rgse.timecap.view.util;

import java.awt.Font;
import java.awt.FontFormatException;
import java.io.IOException;
import java.io.InputStream;

import org.apache.log4j.Logger;

/**
 * @author Gabanna
 * @version $Revision: 1.0 $
 */
public class FontUtil {

    private static final Logger LOGGER = Logger.getLogger(FontUtil.class);

    /**
     * Method getFont.
     * 
     * 
     * 
     * @return Font
     */
    public static Font getFont() {
        Font font = null;
        try {
            InputStream stream = ClassLoader.getSystemResourceAsStream("bewilder.ttf");
            font = Font.createFont(Font.TRUETYPE_FONT, stream);
            font = font.deriveFont(Font.BOLD, 45);
        } catch (FontFormatException e) {
            FontUtil.LOGGER.error(e);
        } catch (IOException e) {
            FontUtil.LOGGER.error(e);
        }
        return font;
    }
}
