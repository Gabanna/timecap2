package de.rgse.timecap.view.pages;

import java.awt.BorderLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Date;

import javax.swing.BorderFactory;
import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JSpinner;

import org.jdesktop.swingx.JXTitledPanel;

import de.rgse.timecap.model.ReportObjekt;
import de.rgse.timecap.view.FrameTemplate;
import de.rgse.timecap.view.data.VertretungsplanerData;
import de.rgse.timecap.view.util.IconFactory;
import de.rgse.timecap.view.util.IconGroesseEnum;
import de.rgse.timecap.view.util.ViewUtil;

public class VertretungsplanerPage extends FrameTemplate {

    private static final long serialVersionUID = 1L;

    private final VertretungsplanerData data;

    private JSpinner vonPicker, bisPicker;

    public VertretungsplanerPage(ReportObjekt reportObjekt) {
        super("Vertretungsplaner", IconFactory.getVertretung(IconGroesseEnum.PX32));
        data = new VertretungsplanerData(reportObjekt);

        JPanel panel = new JPanel(new BorderLayout());
        FrameTemplate.initComponent(panel);
        panel.add(BorderLayout.CENTER, createCenterPanel());
        panel.add(BorderLayout.SOUTH, createSouthPanel());

        JXTitledPanel contentPane = new JXTitledPanel(getTitle(), panel);
        FrameTemplate.initComponent(contentPane);
        setContentPane(contentPane);

        contentPane.setBorder(BorderFactory.createEmptyBorder(20, 20, 20, 20));

        pack();
        setVisible(true);
    }

    private JPanel createCenterPanel() {
        JLabel label = new JLabel("Möchten Sie die Vertretungen für die folgenden Tage eintragen?");
        FrameTemplate.initComponent(label);

        final JPanel datePanel = new JPanel(new GridLayout(1, 2));

        // RadioButton erstellen
        JPanel radioPanel = initRadioButton(datePanel);

        JPanel titlePanel = new JPanel(new GridLayout(2, 1));
        titlePanel.add(label);
        titlePanel.add(radioPanel);

        // Datepicker erstellen
        initDatePicker(datePanel);

        JPanel panel = new JPanel(new GridLayout(0, 1));
        FrameTemplate.initComponent(panel);

        panel.add(titlePanel);
        panel.add(datePanel);

        return panel;
    }

    private JPanel initRadioButton(final JPanel datePanel) {
        JRadioButton jaRBtn = new JRadioButton("Ja");
        jaRBtn.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                datePanel.setEnabled(true);
            }
        });
        JRadioButton neinRBtn = new JRadioButton("Nein");
        neinRBtn.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                datePanel.setEnabled(false);
            }
        });

        ButtonGroup group = new ButtonGroup();
        group.add(jaRBtn);
        group.add(neinRBtn);

        neinRBtn.setSelected(true);

        JPanel radioPanel = new JPanel(new GridLayout(1, 2));
        radioPanel.add(jaRBtn);
        radioPanel.add(neinRBtn);
        return radioPanel;
    }

    private void initDatePicker(final JPanel datePanel) {
        vonPicker = ViewUtil.createDatePicker(data.getVonCalendar());
        FrameTemplate.initComponent(vonPicker);
        bisPicker = ViewUtil.createDatePicker(data.getBisCalendar());
        FrameTemplate.initComponent(bisPicker);

        datePanel.add(vonPicker);
        datePanel.add(bisPicker);
    }

    private JPanel createSouthPanel() {

        JButton okBtn = new JButton("OK", IconFactory.getOK(IconGroesseEnum.PX16));
        FrameTemplate.initComponent(okBtn);
        okBtn.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                data.okAction((Date) vonPicker.getValue(), (Date) bisPicker.getValue());
                dispose();
            }
        });

        JButton abbrechenButton = new JButton("Abbrechen", IconFactory.getCancle(IconGroesseEnum.PX16));
        FrameTemplate.initComponent(abbrechenButton);
        abbrechenButton.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                dispose();
            }
        });

        JPanel panel = new JPanel(new GridLayout(1, 0));
        FrameTemplate.initComponent(panel);
        panel.add(okBtn);
        panel.add(abbrechenButton);
        return panel;
    }
}
