package de.rgse.timecap.view.data;

import java.awt.GridLayout;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Locale;

import javax.swing.JOptionPane;
import javax.swing.JSpinner;

import org.jdesktop.swingx.JXLabel;
import org.jdesktop.swingx.JXPanel;

import de.rgse.timecap.model.Arbeitstag;
import de.rgse.timecap.model.Pause;
import de.rgse.timecap.model.ReportObjekt;
import de.rgse.timecap.service.ServiceProvider;
import de.rgse.timecap.service.TagAnalyseService;
import de.rgse.timecap.service.TagBeginnenService;
import de.rgse.timecap.view.FrameTemplate;
import de.rgse.timecap.view.util.IconFactory;
import de.rgse.timecap.view.util.IconGroesseEnum;
import de.rgse.timecap.view.util.ViewUtil;

public class DetailData {
	private Arbeitstag arbeitstag;
	private final ReportObjekt reportObjekt;
	private final TagAnalyseService tagAnalyseService = ServiceProvider
			.getTagAnalyseServiceInstance();
	private final TagBeginnenService tagBeginnenService = ServiceProvider
			.getTagBeginnenService();
	private JSpinner monthPicker;

	public DetailData() {
		JOptionPane.showMessageDialog(null, createMessage(),
				"Arbeitstag erstellen", JOptionPane.QUESTION_MESSAGE,
				IconFactory.getDate(IconGroesseEnum.PX32));

		GregorianCalendar calendar = new GregorianCalendar();
		calendar.setTime((Date) monthPicker.getValue());

		arbeitstag = tagAnalyseService.findBy(calendar);

		if (null == arbeitstag) {
			arbeitstag = tagBeginnenService.erstelleTag(calendar);
		}
		reportObjekt = tagAnalyseService
				.findReportObjektByArbeitstag(arbeitstag);
	}

	private JXPanel createMessage() {
		JXLabel text = new JXLabel(
				"Bitte geben Sie das Datum für den Arbeitstag an");
		FrameTemplate.initComponent(text);

		GregorianCalendar date = new GregorianCalendar(Locale.GERMANY);
		date.set(Calendar.DATE, date.get(Calendar.DATE) + 1);
		monthPicker = ViewUtil.getDatePicker(date);
		FrameTemplate.initComponent(monthPicker);

		JXPanel panel = new JXPanel(new GridLayout(2, 0));
		FrameTemplate.initComponent(panel);
		panel.add(text);
		panel.add(monthPicker);

		return panel;
	}

	public DetailData(long id) {
		arbeitstag = tagAnalyseService.findBy(id);
		reportObjekt = tagAnalyseService
				.findReportObjektByArbeitstag(arbeitstag);
	}

	public String getTitle() {
		return arbeitstag.getDatumAsString();
	}

	public String getPausenEnde() {
		String result = "";
		if (null != arbeitstag.getPause()) {
			result = arbeitstag.getPause().getEnde();
		}
		return result;
	}

	public String getPausenbeginn() {
		String result = "";
		if (null != arbeitstag.getPause()) {
			result = arbeitstag.getPause().getStart();
		}
		return result;
	}

	public String getArbeitsende() {
		return arbeitstag.getTagesEnde();
	}

	public String getArbeitsbeginn() {
		return arbeitstag.getTagesStart();
	}

	public boolean isVertretungstag() {
		return arbeitstag.isVertretungstag();
	}

	public String getReportTagesBeginn() {
		return reportObjekt.getTagesStart();
	}

	public String getReportTagesEnde() {
		return reportObjekt.getTagesEnde();
	}

	public String getReportPausenBeginn() {
		String result = "";
		if (null != reportObjekt.getPause()) {
			result = reportObjekt.getPause().getStart();
		}
		return result;
	}

	public String getReportPausenEnde() {
		String result = "";
		if (null != reportObjekt.getPause()) {
			result = reportObjekt.getPause().getEnde();

		}
		return result;
	}

	public void setArbeitsbeginn(String text) {
		arbeitstag.setTagesStart(text);
		tagAnalyseService.update(arbeitstag);
	}

	public void setArbeitsende(String text) {
		arbeitstag.setTagesEnde(text);
		tagAnalyseService.update(arbeitstag);
	}

	public void setPausenbeginn(String text) {
		if (null == arbeitstag.getPause()) {
			arbeitstag.setPause(new Pause(arbeitstag, text));
		} else {
			arbeitstag.getPause().setStart(text);
		}
		tagAnalyseService.update(arbeitstag);
	}

	public void setPausenende(String text) {
		if (null == arbeitstag.getPause()) {
			arbeitstag.setPause(new Pause(arbeitstag, text));
		} else {
			arbeitstag.getPause().setStart(text);
		}
		arbeitstag.getPause().setEnde(text);
		tagAnalyseService.update(arbeitstag);
	}

	public void setReportArbeitsbeginn(String text) {
		reportObjekt.setTagesStart(text);
		tagAnalyseService.update(reportObjekt);
	}

	public void setReportArbeitsende(String text) {
		reportObjekt.setTagesEnde(text);
		tagAnalyseService.update(reportObjekt);
	}
}
