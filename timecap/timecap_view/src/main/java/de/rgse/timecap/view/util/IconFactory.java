package de.rgse.timecap.view.util;

import javax.swing.ImageIcon;

public abstract class IconFactory {

    public static ImageIcon getLogoIcon() {
        return new ImageIcon(ClassLoader.getSystemResource("img/Logo_icon.png"));
    }

    public static ImageIcon getLogoIconSmall() {
        return new ImageIcon(ClassLoader.getSystemResource("img/Logo_icon_small.png"));
    }

    public static ImageIcon getLogoIconTray() {
        return new ImageIcon(ClassLoader.getSystemResource("img/Logo_icon_tray.png"));
    }

    public static ImageIcon getCoffee() {
        return new ImageIcon(ClassLoader.getSystemResource("img/cup_anim_e0.gif"));
    }

    public static ImageIcon getExitOption(IconGroesseEnum groesse) {
        return new ImageIcon(ClassLoader.getSystemResource("img/" + groesse.getGroesse() + "/exit.png"));
    }

    public static ImageIcon getUeberblick(IconGroesseEnum groesse) {
        return new ImageIcon(ClassLoader.getSystemResource("img/" + groesse.getGroesse() + "/fsview.png"));
    }

    public static ImageIcon getQuestion(IconGroesseEnum groesse) {
        return new ImageIcon(ClassLoader.getSystemResource("img/" + groesse.getGroesse() + "/help.png"));
    }

    public static ImageIcon getOK(IconGroesseEnum groesse) {
        return new ImageIcon(ClassLoader.getSystemResource("img/" + groesse.getGroesse() + "/apply.png"));
    }

    public static ImageIcon getCancle(IconGroesseEnum groesse) {
        return new ImageIcon(ClassLoader.getSystemResource("img/" + groesse.getGroesse() + "/cancel.png"));
    }

    public static ImageIcon getPencil(IconGroesseEnum groesse) {
        return new ImageIcon(ClassLoader.getSystemResource("img/" + groesse.getGroesse() + "/pencil.png"));
    }

    public static ImageIcon getDate(IconGroesseEnum groesse) {
        return new ImageIcon(ClassLoader.getSystemResource("img/" + groesse.getGroesse() + "/date.png"));
    }

    public static ImageIcon getBug(IconGroesseEnum groesse) {
        return new ImageIcon(ClassLoader.getSystemResource("img/" + groesse.getGroesse() + "/bug.png"));
    }

    public static ImageIcon getMail(IconGroesseEnum groesse) {
        return new ImageIcon(ClassLoader.getSystemResource("img/" + groesse.getGroesse() + "/email.png"));
    }

    public static ImageIcon getWarning(IconGroesseEnum groesse) {
        return new ImageIcon(ClassLoader.getSystemResource("img/" + groesse.getGroesse() + "/messagebox_warning.png"));
    }

    public static ImageIcon getExport(IconGroesseEnum groesse) {
        return new ImageIcon(ClassLoader.getSystemResource("img/" + groesse.getGroesse() + "/export.png"));
    }

    public static ImageIcon getCopy(IconGroesseEnum groesse) {
        return new ImageIcon(ClassLoader.getSystemResource("img/" + groesse.getGroesse() + "/editpaste.png"));
    }

    public static ImageIcon getHwinfoIcon(IconGroesseEnum groesse) {
        return new ImageIcon(ClassLoader.getSystemResource("img/" + groesse.getGroesse() + "/hwinfo.png"));
    }

    public static ImageIcon getBusy() {
        return new ImageIcon(ClassLoader.getSystemResource("img/package_system.png"));
    }

    public static ImageIcon getInfoOption(IconGroesseEnum groesse) {
        return new ImageIcon(ClassLoader.getSystemResource("img/" + groesse.getGroesse() + "/messagebox_info.png"));
    }

    public static ImageIcon getNew(IconGroesseEnum groesse) {
        return new ImageIcon(ClassLoader.getSystemResource("img/" + groesse.getGroesse() + "/edit_add.png"));
    }

    public static ImageIcon getSave(IconGroesseEnum groesse) {
        return new ImageIcon(ClassLoader.getSystemResource("img/" + groesse.getGroesse() + "/filesave.png"));
    }

    public static ImageIcon getRemove(IconGroesseEnum groesse) {
        return new ImageIcon(ClassLoader.getSystemResource("img/" + groesse.getGroesse() + "/edittrash.png"));
    }

    public static ImageIcon getChart(IconGroesseEnum groesse) {
        return new ImageIcon(ClassLoader.getSystemResource("img/" + groesse.getGroesse() + "/kcmpartitions.png"));
    }

    public static ImageIcon getVertretung(IconGroesseEnum groesse) {
        return new ImageIcon(ClassLoader.getSystemResource("img/" + groesse.getGroesse() + "/kdmconfig.png"));
    }

}
