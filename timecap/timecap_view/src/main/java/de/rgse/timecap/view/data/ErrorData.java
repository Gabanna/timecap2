package de.rgse.timecap.view.data;

import java.awt.Component;
import java.awt.Toolkit;
import java.awt.datatransfer.StringSelection;
import java.awt.event.ActionEvent;

import javax.swing.JOptionPane;

import org.apache.commons.lang.exception.ExceptionUtils;

import de.rgse.timecap.view.util.IconFactory;
import de.rgse.timecap.view.util.IconGroesseEnum;

public class ErrorData {

    private final Throwable throwable;

    public ErrorData(Throwable throwable) {
        this.throwable = throwable;
    }

    public Throwable getThrowable() {
        return throwable;
    }

    public void zwischenablageAction(ActionEvent e) {
        Toolkit.getDefaultToolkit().getSystemClipboard().setContents(new StringSelection(getStacktrace()), null);
        JOptionPane.showMessageDialog(((Component) e.getSource()).getParent(), "In Zwischenablage gelegt",
                "Zwischenablage", JOptionPane.INFORMATION_MESSAGE, IconFactory.getCopy(IconGroesseEnum.PX32));
    }

    public String getStacktrace() {
        return ExceptionUtils.getStackTrace(throwable);
    }

}
