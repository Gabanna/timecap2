package de.rgse.timecap.view.util;

public enum IconGroesseEnum {

    PX16(16), PX32(32);

    private int groesse;

    private IconGroesseEnum(int groesse) {
        this.groesse = groesse;
    }

    public String getGroesse() {
        return String.valueOf(groesse) + "px";
    }
}
