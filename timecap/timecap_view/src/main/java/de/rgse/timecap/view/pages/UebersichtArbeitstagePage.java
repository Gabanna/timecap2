package de.rgse.timecap.view.pages;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.text.ParseException;
import java.util.Date;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSpinner;
import javax.swing.JTable;
import javax.swing.SwingConstants;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import org.jdesktop.swingx.JXHyperlink;
import org.jdesktop.swingx.JXPanel;
import org.jdesktop.swingx.JXTitledPanel;

import de.rgse.timecap.view.FrameTemplate;
import de.rgse.timecap.view.data.UebersichtArbeitstageData;
import de.rgse.timecap.view.util.IconFactory;
import de.rgse.timecap.view.util.IconGroesseEnum;
import de.rgse.timecap.view.util.TableModel;
import de.rgse.timecap.view.util.ViewUtil;

public class UebersichtArbeitstagePage extends FrameTemplate {

    private static final long serialVersionUID = 2628217992900177455L;

    private static String title = "Übersicht der Arbeitstage";

    private final UebersichtArbeitstageData data;

    private JTable table;

    public UebersichtArbeitstagePage() {
        super(UebersichtArbeitstagePage.title, IconFactory.getUeberblick(IconGroesseEnum.PX32));
        data = new UebersichtArbeitstageData(this);

        JPanel panel = new JPanel(new BorderLayout());
        FrameTemplate.initComponent(panel);

        panel.add(BorderLayout.CENTER, createCenterPanel());
        panel.add(BorderLayout.SOUTH, createSouthPanel());

        JXTitledPanel contentPane = new JXTitledPanel(getTitle(), panel);
        FrameTemplate.initComponent(contentPane);

        contentPane.setBorder(BorderFactory.createEmptyBorder(20, 20, 20, 20));

        setContentPane(contentPane);
        pack();
        setVisible(true);
    }

    private JPanel createCenterPanel() {
        JPanel panel = new JPanel(new BorderLayout());
        FrameTemplate.initComponent(panel);

        JPanel tablePanel = new JPanel(new GridLayout(0, 1));
        FrameTemplate.initComponent(tablePanel);

        table = new JTable(data.getTableModel());
        FrameTemplate.initComponent(table);
        table.setAutoResizeMode(JTable.AUTO_RESIZE_ALL_COLUMNS);
        table.addMouseListener(getTableMouseListener(table));

        tablePanel.add(new JScrollPane(table));

        final JSpinner picker = ViewUtil.getMonthPicker(data.getSelectedMonth());
        picker.addChangeListener(getPickerChangeListener());
        data.updateModel((Date) picker.getValue());

        JLabel label = new JLabel("Ausgewählter Monat:", IconFactory.getDate(IconGroesseEnum.PX16),
                SwingConstants.HORIZONTAL);
        FrameTemplate.initComponent(label);

        JXPanel monthPickerPanel = new JXPanel();
        FrameTemplate.initComponent(monthPickerPanel);
        monthPickerPanel.add(label);
        monthPickerPanel.add(picker);

        final JXHyperlink chartLink = new JXHyperlink();
        chartLink.setText("Verlauf anzeigen");
        chartLink.addActionListener(getChartActionListener());

        chartLink.setForeground(Color.BLUE);
        chartLink.setHorizontalAlignment(SwingConstants.CENTER);
        chartLink.addMouseListener(getLinkMouseListener(chartLink));

        panel.add(BorderLayout.NORTH, monthPickerPanel);
        panel.add(BorderLayout.CENTER, tablePanel);
        panel.add(BorderLayout.SOUTH, chartLink);
        return panel;
    }

    private MouseAdapter getTableMouseListener(final JTable table) {
        return new MouseAdapter() {

            @Override
            public void mouseClicked(MouseEvent e) {
                if (e.getClickCount() >= 2) {
                    long id = (long) ((JTable) e.getSource()).getValueAt(table.getSelectedRow(), 0);
                    data.showDetails(id);
                    dispose();
                }
            }
        };
    }

    private ChangeListener getPickerChangeListener() {
        return new ChangeListener() {

            @Override
            public void stateChanged(ChangeEvent e) {
                data.updateModel((Date) ((JSpinner) e.getSource()).getValue());
                repaint();
            }
        };
    }

    private ActionListener getChartActionListener() {
        return new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    new ChartPane(data.getDays());
                    dispose();
                } catch (ParseException e1) {
                    new ErrorPage(e1);
                }
            }
        };
    }

    private MouseAdapter getLinkMouseListener(final JXHyperlink chartLink) {
        return new MouseAdapter() {

            private final Font highlightFont = new Font(chartLink.getFont().getName(), Font.BOLD, chartLink.getFont()
                    .getSize());

            private final Font normalFont = chartLink.getFont();

            @Override
            public void mouseEntered(MouseEvent e) {
                super.mouseEntered(e);
                chartLink.setFont(highlightFont);
            }

            @Override
            public void mouseExited(MouseEvent e) {
                super.mouseExited(e);
                chartLink.setFont(normalFont);
            }
        };
    }

    private JPanel createSouthPanel() {

        JButton removeDay = new JButton("Arbeitstag löschen");
        FrameTemplate.initComponent(removeDay);
        removeDay.setIcon(IconFactory.getRemove(IconGroesseEnum.PX16));
        removeDay.addActionListener(getRemoveAction());

        JButton newDayBtn = new JButton("Arbeitstag erstellen");
        FrameTemplate.initComponent(newDayBtn);
        newDayBtn.setIcon(IconFactory.getNew(IconGroesseEnum.PX16));
        newDayBtn.addActionListener(getNewDayAction());

        JButton schliessenBtn = new JButton("Schließen");
        FrameTemplate.initComponent(schliessenBtn);
        schliessenBtn.setIcon(IconFactory.getCancle(IconGroesseEnum.PX16));
        schliessenBtn.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                dispose();
            }
        });

        JPanel content = new JPanel(new GridLayout(1, 0));
        content.add(newDayBtn);
        content.add(removeDay);
        content.add(schliessenBtn);

        JPanel panel = new JPanel(new BorderLayout());
        FrameTemplate.initComponent(panel);
        panel.add(BorderLayout.EAST, content);

        return panel;
    }

    private ActionListener getRemoveAction() {
        return new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    if (table.getSelectedRow() >= 0) {
                        long id = (long) data.getTableModel().getValueAt(table.getSelectedRow(), TableModel.ID_COLUMN);
                        data.removeDay(id);
                        data.updateModel();
                        validate();
                    }
                } catch (Exception ex) {
                    new ErrorPage(ex);
                    dispose();
                }
            }
        };
    }

    private ActionListener getNewDayAction() {
        return new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                data.showDetails();
                dispose();
            }
        };
    }
}
