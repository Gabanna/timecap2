package de.rgse.timecap.view.pages;

import java.awt.Cursor;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.Point;
import java.awt.Toolkit;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingConstants;

import org.jdesktop.swingx.JXFrame;

import de.rgse.timecap.view.FrameTemplate;
import de.rgse.timecap.view.util.FontUtil;
import de.rgse.timecap.view.util.IconFactory;
import de.rgse.timecap.view.util.ViewUtil;

/**
 * @author Gabanna
 * @version $Revision: 1.0 $
 */
public class SplashScreen extends FrameTemplate {

    private static final long serialVersionUID = 3608368808892509913L;

    public SplashScreen() {
        super();
        addMouseListener(getMouseListener(this));
        setUndecorated(true);
        setBackground(ViewUtil.getGreencolor());
        JLabel label = new JLabel(IconFactory.getLogoIcon());
        label.setOpaque(false);

        JLabel textLbl = new JLabel("TimeCap");
        textLbl.setFont(FontUtil.getFont());
        textLbl.setForeground(ViewUtil.getGreencolor());
        textLbl.setHorizontalAlignment(SwingConstants.CENTER);

        JLabel textLbl2 = new JLabel("Zeiterfassung");
        textLbl2.setFont(FontUtil.getFont());
        textLbl2.setForeground(ViewUtil.getGreencolor());
        textLbl2.setHorizontalAlignment(SwingConstants.CENTER);

        JLabel textLbl3 = new JLabel("Version 2.0");
        textLbl2.setFont(FontUtil.getFont());
        textLbl2.setForeground(ViewUtil.getGreencolor());
        textLbl2.setHorizontalAlignment(SwingConstants.CENTER);

        JLabel textLbl4 = new JLabel("wird gestartet...");
        textLbl2.setHorizontalAlignment(SwingConstants.CENTER);

        GridBagConstraints cons = new GridBagConstraints();
        cons.gridheight = 1;
        cons.insets = new Insets(10, 5, 10, 5);

        JPanel panel = new JPanel(new GridBagLayout());
        FrameTemplate.initComponent(panel);
        cons.gridy = 0;
        panel.add(label, cons);
        cons.gridy++;
        panel.add(textLbl, cons);
        cons.gridy++;
        panel.add(textLbl2, cons);
        cons.gridy++;
        panel.add(textLbl3, cons);
        cons.gridy++;
        panel.add(textLbl4, cons);
        panel.setBorder(BorderFactory.createLineBorder(ViewUtil.getGreencolor()));

        getContentPane().add(panel);
        pack();
        setLocationRelativeTo(null);
        setVisible(true);
    }

    private MouseListener getMouseListener(final JXFrame frame) {
        return new MouseAdapter() {

            @Override
            public void mouseEntered(MouseEvent e) {
                frame.setCursor(getBusyCursor());
            }

            @Override
            public void mouseExited(MouseEvent e) {
                frame.setCursor(Cursor.getDefaultCursor());
            }
        };
    }

    private Cursor getBusyCursor() {
        Toolkit toolkit = Toolkit.getDefaultToolkit();
        ImageIcon icon = IconFactory.getBusy();

        Point hotSpot = new Point(icon.getIconHeight() / 2, icon.getIconWidth() / 2);
        return toolkit.createCustomCursor(icon.getImage(), hotSpot, "busy");
    }
}
