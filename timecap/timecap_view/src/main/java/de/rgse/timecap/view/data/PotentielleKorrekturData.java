package de.rgse.timecap.view.data;

import de.rgse.timecap.model.ReportObjekt;
import de.rgse.timecap.service.ServiceProvider;

public class PotentielleKorrekturData {

    public void getBeginnKorrigierenAction(ReportObjekt reportObjekt) {
        ServiceProvider.getTagBeginnenService().reportObjectBeginnen(reportObjekt, "10:00");
    }

    public void getEndeKorrigierenAction(ReportObjekt reportObjekt) {
        ServiceProvider.getTagBeendenServiceInstance().reportObjectBeenden(reportObjekt, "18:00");
    }

}
