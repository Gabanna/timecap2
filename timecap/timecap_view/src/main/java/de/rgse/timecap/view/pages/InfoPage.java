package de.rgse.timecap.view.pages;

import java.awt.BorderLayout;
import java.awt.GridLayout;

import javax.swing.BorderFactory;
import javax.swing.JTextArea;
import javax.swing.WindowConstants;

import org.jdesktop.swingx.JXLabel;
import org.jdesktop.swingx.JXPanel;
import org.jdesktop.swingx.JXTitledPanel;

import de.rgse.timecap.view.FrameTemplate;
import de.rgse.timecap.view.data.InfoData;
import de.rgse.timecap.view.util.IconFactory;
import de.rgse.timecap.view.util.IconGroesseEnum;

public class InfoPage extends FrameTemplate {

    private static final long serialVersionUID = 1L;

    private final InfoData infodata;

    private JXLabel icon;

    private JXPanel infoPanel;

    public InfoPage() {
        super("Info", IconFactory.getInfoOption(IconGroesseEnum.PX32));
        infodata = new InfoData();
        erzeugeComponenten();
        layoutComponenten();
        initFrame();
    }

    private void erzeugeComponenten() {
        JXLabel user = new JXLabel(infodata.user);
        FrameTemplate.initComponent(user);

        JXLabel os = new JXLabel(infodata.osName + " v" + infodata.osVersion + " x" + infodata.osArch);
        FrameTemplate.initComponent(os);

        JXLabel javaInfo = new JXLabel(infodata.javaVersion);
        FrameTemplate.initComponent(javaInfo);

        JXLabel javaHome = new JXLabel(infodata.javaHome);
        FrameTemplate.initComponent(javaHome);

        JTextArea javaPath = new JTextArea(infodata.javaClassPath);
        FrameTemplate.initComponent(javaPath);
        javaPath.setLineWrap(true);
        javaPath.setEditable(false);

        JXLabel buildVersion = new JXLabel(infodata.buildVersion);
        FrameTemplate.initComponent(buildVersion);

        JXLabel credits = new JXLabel(infodata.credits);
        FrameTemplate.initComponent(credits);

        JXLabel leer = new JXLabel();
        FrameTemplate.initComponent(leer);

        JXPanel data = new JXPanel(new GridLayout(0, 1));
        FrameTemplate.initComponent(data);
        data.add(user);
        data.add(os);
        data.add(javaInfo);
        data.add(javaHome);
        data.add(javaPath);
        data.add(buildVersion);
        data.add(leer);

        JXLabel userInfoLbl = new JXLabel("Benutzer:");
        JXLabel osInfoLbl = new JXLabel("Betriebssystem:");
        JXLabel javaInfoLbl = new JXLabel("Java-Version:");
        JXLabel javaHomeLbl = new JXLabel("Java-Verzeichnis:");
        JXLabel javaPathLbl = new JXLabel("Anwendungsverzeichnis:");
        JXLabel buildInfoLbl = new JXLabel("Timecap-Version:");

        JXPanel names = new JXPanel(new GridLayout(0, 1));
        FrameTemplate.initComponent(names);
        names.add(userInfoLbl);
        names.add(osInfoLbl);
        names.add(javaInfoLbl);
        names.add(javaHomeLbl);
        names.add(javaPathLbl);
        names.add(buildInfoLbl);
        names.add(credits);

        infoPanel = new JXPanel(new GridLayout(0, 2, 10, 10));
        FrameTemplate.initComponent(infoPanel);
        infoPanel.add(names);
        infoPanel.add(data);

        icon = new JXLabel(IconFactory.getHwinfoIcon(IconGroesseEnum.PX32));
        FrameTemplate.initComponent(icon);
    }

    private void layoutComponenten() {
        JXPanel contentPanel = new JXPanel(new BorderLayout(20, 20));
        FrameTemplate.initComponent(contentPanel);
        contentPanel.add(BorderLayout.WEST, icon);
        contentPanel.add(BorderLayout.CENTER, infoPanel);

        JXTitledPanel titledPanel = new JXTitledPanel(infodata.title, contentPanel);
        FrameTemplate.initComponent(titledPanel);

        titledPanel.setBorder(BorderFactory.createEmptyBorder(20, 20, 20, 20));

        setContentPane(titledPanel);
    }

    private void initFrame() {
        setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
        setVisible(true);
    }

    public static void main(String[] args) {
        new InfoPage();
    }
}
