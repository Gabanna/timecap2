package de.rgse.timecap.view.util;

/*
 * Copyright 2008 Sun Microsystems, Inc., 4150 Network Circle, Santa Clara,
 * California 95054, U.S.A. All rights reserved.
 * 
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 * 
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with this library; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA
 */

import java.awt.Dimension;
import java.awt.Frame;
import java.awt.Image;
import java.awt.TrayIcon;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.JDialog;
import javax.swing.JPopupMenu;
import javax.swing.event.PopupMenuEvent;
import javax.swing.event.PopupMenuListener;

/**
 * @author Gabanna
 * @version $Revision: 1.0 $
 */
public class JTrayIcon extends TrayIcon {

    private JPopupMenu menu;

    private static JDialog dialog;
    static {
        JTrayIcon.dialog = new JDialog((Frame) null);
        JTrayIcon.dialog.setUndecorated(true);
        JTrayIcon.dialog.setAlwaysOnTop(true);
    }

    private static PopupMenuListener popupListener = new PopupMenuListener() {

        @Override
        public void popupMenuCanceled(PopupMenuEvent e) {
            JTrayIcon.dialog.setVisible(false);
        }

        @Override
        public void popupMenuWillBecomeInvisible(PopupMenuEvent e) {
            JTrayIcon.dialog.setVisible(false);
        }

        @Override
        public void popupMenuWillBecomeVisible(PopupMenuEvent e) {
        }
    };

    /**
     * Constructor for JTrayIcon.
     * 
     * @param image
     *            Image
     */
    public JTrayIcon(Image image) {
        super(image);
        setImageAutoSize(true);
        addMouseListener(new MouseAdapter() {

            @Override
            public void mousePressed(MouseEvent e) {
                showJPopupMenu(e);
            }

            @Override
            public void mouseReleased(MouseEvent e) {
                showJPopupMenu(e);
            }
        });
    }

    /**
     * Method getJPopupMenu.
     * 
     * 
     * 
     * @return JPopupMenu
     */
    public JPopupMenu getJPopupMenu() {
        return menu;
    }

    /**
     * Method setJPopupMenu.
     * 
     * @param menu
     *            JPopupMenu
     */
    public void setJPopupMenu(JPopupMenu menu) {
        if (this.menu != null) {
            this.menu.removePopupMenuListener(JTrayIcon.popupListener);
        }
        this.menu = menu;
        menu.addPopupMenuListener(JTrayIcon.popupListener);
    }

    /**
     * Method showJPopupMenu.
     * 
     * @param x
     *            int
     * @param y
     *            int
     */
    protected void showJPopupMenu(int x, int y) {
        JTrayIcon.dialog.setLocation(x, y);
        JTrayIcon.dialog.setVisible(true);
        menu.show(JTrayIcon.dialog.getContentPane(), 0, 0);
        // popup works only for focused windows
        JTrayIcon.dialog.toFront();
    }

    /**
     * Method showJPopupMenu.
     * 
     * @param e
     *            MouseEvent
     */
    protected void showJPopupMenu(MouseEvent e) {
        if (e.isPopupTrigger() && menu != null) {
            Dimension size = menu.getPreferredSize();
            showJPopupMenu(e.getX(), e.getY() - size.height);
        }
    }
}
