package de.rgse.timecap.view.util;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

import javax.swing.table.DefaultTableModel;

import org.apache.log4j.Logger;

import de.rgse.timecap.model.Arbeitstag;
import de.rgse.timecap.service.TagAnalyseService;

public class TableModel extends DefaultTableModel {

    public TagAnalyseService getTagAnalyseService() {
        return tagAnalyseService;
    }

    private static final long serialVersionUID = 1L;

    private static final Logger LOGGER = Logger.getLogger(TableModel.class);

    private List<Arbeitstag> arbeitstage;

    public static final int ID_COLUMN = 0;

    public static final int DATUM_COLUMN = 1;

    public static final int TAGES_BEGINN_COLUMN = 2;

    public static final int TAGES_ENDE_COLUMN = 3;

    public static final int PAUSEN_BEGINN_COLUMN = 4;

    public static final int PAUSEN_ENDE_COLUMN = 5;

    private final TagAnalyseService tagAnalyseService;

    public TableModel(TagAnalyseService tagAnalyseService) {
        this.tagAnalyseService = tagAnalyseService;
    }

    @Override
    public boolean isCellEditable(int row, int column) {
        return false;
    }

    @Override
    public void setValueAt(Object aValue, int row, int column) {
        Arbeitstag arbeitstag = arbeitstage.get(row);

        try {
            new SimpleDateFormat("kk:mm").parse((String) aValue);
            String change = (String) aValue;

            switch (column) {
                case 2:
                    arbeitstag.setTagesStart(change);
                    break;
                case 3:
                    arbeitstag.setTagesEnde(change);
                    break;
            }
            tagAnalyseService.update(arbeitstag);
        } catch (ParseException e) {
            TableModel.LOGGER.error(e);
        }
    }

    public void update(Date date) {
        GregorianCalendar calendar = new GregorianCalendar();
        calendar.setTime(date);
        arbeitstage = tagAnalyseService.findAllByMonth(calendar);

        setRowCount(0);

        for (Arbeitstag arbeitstag : arbeitstage) {
            Object[] data = new Object[6];
            data[0] = arbeitstag.getId();
            data[1] = arbeitstag.getDatumAsString();
            data[2] = arbeitstag.getTagesStart();
            data[3] = arbeitstag.getTagesEnde();

            if (null != arbeitstag.getPause()) {
                data[4] = arbeitstag.getPause().getStart();
                data[5] = arbeitstag.getPause().getEnde();
            }

            addRow(data);
        }
    }

    public List<Arbeitstag> getArbeitstage() {
        return arbeitstage;
    }
}
