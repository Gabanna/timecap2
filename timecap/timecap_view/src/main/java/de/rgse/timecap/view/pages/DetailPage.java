package de.rgse.timecap.view.pages;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import javax.swing.BorderFactory;
import javax.swing.JCheckBox;
import javax.swing.JFormattedTextField;
import javax.swing.WindowConstants;

import org.jdesktop.swingx.JXButton;
import org.jdesktop.swingx.JXLabel;
import org.jdesktop.swingx.JXPanel;
import org.jdesktop.swingx.JXTitledPanel;

import de.rgse.timecap.view.FrameTemplate;
import de.rgse.timecap.view.data.DetailData;
import de.rgse.timecap.view.util.IconFactory;
import de.rgse.timecap.view.util.IconGroesseEnum;

public class DetailPage extends FrameTemplate {

	private static final long serialVersionUID = 1L;

	private static final SimpleDateFormat FORMATTER = new SimpleDateFormat(
			"kk:mm");

	private final DetailData data;

	private JXPanel arbeitstagPanel, reportObjectPanel, controlPanel;

	private List<JFormattedTextField> textFields, reportObjectTextFields;

	public DetailPage() {
		super("Detailansicht", IconFactory.getUeberblick(IconGroesseEnum.PX32));
		data = new DetailData();

		erzeugeComponenten();
		layoutComponenten();
		initFrame();
	}

	public DetailPage(long id) {
		super("Detailansicht", IconFactory.getUeberblick(IconGroesseEnum.PX32));
		data = new DetailData(id);

		erzeugeComponenten();
		layoutComponenten();
		initFrame();
	}

	private void initFrame() {
		setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
		setVisible(true);
	}

	private void erzeugeComponenten() {
		arbeitstagPanel = erzeugeArbeitstagPanel();
		reportObjectPanel = erzeugeReportObjectPanel();
		controlPanel = erzeugeControlPanel();
	}

	private JXPanel erzeugeControlPanel() {
		JXButton schliessen = new JXButton("Verwerfen",
				IconFactory.getCancle(IconGroesseEnum.PX16));
		schliessen.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				new UebersichtArbeitstagePage();
				dispose();
			}

		});

		JXButton speichern = new JXButton("Speichern",
				IconFactory.getSave(IconGroesseEnum.PX16));
		speichern.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				save();
				new UebersichtArbeitstagePage();
				dispose();
			}

		});

		JXPanel panel = new JXPanel(new FlowLayout(FlowLayout.RIGHT));
		FrameTemplate.initComponent(panel);
		panel.add(speichern);
		panel.add(schliessen);

		return panel;
	}

	private void save() {
		for (JFormattedTextField field : textFields) {
			String text = field.getText();
			String name = field.getName();

			if (name.equals("arbeitsbeginn")) {
				data.setArbeitsbeginn(text);
			} else if (name.equals("arbeitsende")) {
				data.setArbeitsende(text);
			} else if (name.equals("pausenbeginn")) {
				data.setPausenbeginn(text);
			} else if (name.equals("pausenende")) {
				data.setPausenende(text);
			}
		}

		for (JFormattedTextField field : reportObjectTextFields) {
			String text = field.getText();
			String name = field.getName();

			if (name.equals("reporttagesbeginn")) {
				data.setReportArbeitsbeginn(text);
			} else if (name.equals("reporttagesende")) {
				data.setReportArbeitsende(text);
			}
		}
	}

	private void layoutComponenten() {
		JXPanel panel = new JXPanel(new GridLayout(2, 1));
		panel.add(arbeitstagPanel);
		panel.add(reportObjectPanel);

		JXPanel contentPanel = new JXPanel(new BorderLayout());
		contentPanel.add(BorderLayout.CENTER, panel);
		contentPanel.add(BorderLayout.SOUTH, controlPanel);

		JXTitledPanel titledPanel = new JXTitledPanel(data.getTitle(),
				contentPanel);
		FrameTemplate.initComponent(titledPanel);

		titledPanel.setBorder(BorderFactory.createEmptyBorder(20, 20, 20, 20));

		setContentPane(titledPanel);
	}

	private JXPanel erzeugeReportObjectPanel() {
		reportObjectTextFields = new ArrayList<JFormattedTextField>();
		JXLabel tagesbeginnLbl = new JXLabel("Tagesbeginn"), tagesendeLbl = new JXLabel(
				"Tagesende");

		JFormattedTextField tagesbeginnTF = new JFormattedTextField(
				DetailPage.FORMATTER);
		tagesbeginnTF.setText(data.getReportTagesBeginn());
		tagesbeginnTF.setName("reporttagesbeginn");
		reportObjectTextFields.add(tagesbeginnTF);

		JFormattedTextField tagesendeTF = new JFormattedTextField(
				DetailPage.FORMATTER);
		tagesendeTF.setText(data.getReportTagesEnde());
		tagesendeTF.setName("reporttagesende");
		reportObjectTextFields.add(tagesendeTF);

		for (JFormattedTextField jxTextField : reportObjectTextFields) {
			FrameTemplate.initComponent(jxTextField);
		}

		JXPanel panel = new JXPanel(new GridLayout(0, 2));
		FrameTemplate.initComponent(panel);
		panel.setBorder(BorderFactory.createTitledBorder("Reportobjekt"));
		panel.add(tagesbeginnLbl);
		panel.add(tagesbeginnTF);

		panel.add(tagesendeLbl);
		panel.add(tagesendeTF);

		return panel;
	}

	private JXPanel erzeugeArbeitstagPanel() {
		JXLabel arbeitsbeginnLbl = new JXLabel("Arbeitsbeginn"), arbeitsendeLbl = new JXLabel(
				"Arbeitsende"), pausenbeginnLbl = new JXLabel("Pausenbeginn"), pausenendeLbl = new JXLabel(
				"Pausenende");

		textFields = new ArrayList<JFormattedTextField>();

		JFormattedTextField arbeitsbeginnTF = new JFormattedTextField(
				DetailPage.FORMATTER);
		arbeitsbeginnTF.setText(data.getArbeitsbeginn());
		arbeitsbeginnTF.setName("arbeitsbeginn");
		textFields.add(arbeitsbeginnTF);

		JFormattedTextField arbeitsendeTF = new JFormattedTextField(
				DetailPage.FORMATTER);
		arbeitsendeTF.setText(data.getArbeitsende());
		arbeitsendeTF.setName("arbeitsende");
		textFields.add(arbeitsendeTF);

		JFormattedTextField pausenbeginnTF = new JFormattedTextField(
				DetailPage.FORMATTER);
		pausenbeginnTF.setText(data.getPausenbeginn());
		pausenbeginnTF.setName("pausenbeginn");
		textFields.add(pausenbeginnTF);

		JFormattedTextField pausenendeTF = new JFormattedTextField(
				DetailPage.FORMATTER);
		pausenendeTF.setText(data.getPausenEnde());
		pausenendeTF.setName("pausenende");
		textFields.add(pausenendeTF);

		JCheckBox isVertretungCB = new JCheckBox("Vertretungstag",
				data.isVertretungstag());
		FrameTemplate.initComponent(isVertretungCB);
		isVertretungCB.setName("isVertretung");

		for (JFormattedTextField jxTextField : textFields) {
			FrameTemplate.initComponent(jxTextField);
		}

		isVertretungCB.setEnabled(false);

		JXPanel arbeitstagPanel = new JXPanel(new GridLayout(0, 2));
		FrameTemplate.initComponent(arbeitstagPanel);
		arbeitstagPanel.setBorder(BorderFactory
				.createTitledBorder("Arbeitstag"));

		arbeitstagPanel.add(arbeitsbeginnLbl);
		arbeitstagPanel.add(arbeitsbeginnTF);

		arbeitstagPanel.add(arbeitsendeLbl);
		arbeitstagPanel.add(arbeitsendeTF);

		arbeitstagPanel.add(pausenbeginnLbl);
		arbeitstagPanel.add(pausenbeginnTF);

		arbeitstagPanel.add(pausenendeLbl);
		arbeitstagPanel.add(pausenendeTF);

		arbeitstagPanel.add(isVertretungCB);

		return arbeitstagPanel;
	}
}
