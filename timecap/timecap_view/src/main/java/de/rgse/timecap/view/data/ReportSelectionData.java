package de.rgse.timecap.view.data;

import java.awt.Desktop;
import java.io.File;

import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import javax.swing.filechooser.FileNameExtensionFilter;

import org.apache.log4j.Logger;

import de.rgse.timecap.reporting.ReporterService;
import de.rgse.timecap.view.pages.ErrorPage;
import de.rgse.timecap.view.util.IconFactory;
import de.rgse.timecap.view.util.IconGroesseEnum;

public class ReportSelectionData {

	private static final Logger LOGGER = Logger
			.getLogger(ReportSelectionData.class);

	public void reportAction() {
		JFileChooser chooser = new JFileChooser();
		chooser.setFileSelectionMode(JFileChooser.FILES_ONLY);
		chooser.setFileFilter(new FileNameExtensionFilter("Exceldatei", "xls"));

		int confirm = JOptionPane.YES_OPTION;
		while (JOptionPane.YES_OPTION == confirm) {
			if (JFileChooser.APPROVE_OPTION == chooser.showSaveDialog(null)) {
				File file = chooser.getSelectedFile();
				if (file.exists()
						&& file.getName().toLowerCase().endsWith(".xls")) {
					try {
						new ReporterService().reportOfficialDays(file
								.getAbsolutePath());
						showConfirmationDialog();
						Desktop.getDesktop().open(file);
					} catch (Exception e1) {
						ReportSelectionData.LOGGER.error(e1);
						new ErrorPage(e1);
					}
					confirm = -1;
				} else {
					confirm = showNoFileException();
				}
			} else {
				confirm = -1;
			}
		}
	}

	private void showConfirmationDialog() {
		JOptionPane.showMessageDialog(null,
				"Der Bericht wurde erfolgreich erstellt", "Erfolg",
				JOptionPane.INFORMATION_MESSAGE,
				IconFactory.getOK(IconGroesseEnum.PX32));
	}

	private int showNoFileException() {
		return JOptionPane
				.showConfirmDialog(
						null,
						"Die angegebene xls-Datei existiert nicht.\nMöchten Sie eine andere auswählen?",
						"Datei nicht gefunden", JOptionPane.YES_NO_OPTION,
						JOptionPane.QUESTION_MESSAGE,
						IconFactory.getWarning(IconGroesseEnum.PX32));
	}
}
