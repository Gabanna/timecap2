package de.rgse.timecap.view.pages;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.text.ParseException;
import java.util.List;

import javax.swing.BorderFactory;

import org.jdesktop.swingx.JXButton;
import org.jdesktop.swingx.JXPanel;
import org.jfree.chart.ChartPanel;

import de.rgse.timecap.model.Arbeitstag;
import de.rgse.timecap.reporting.ChartService;
import de.rgse.timecap.view.FrameTemplate;
import de.rgse.timecap.view.util.IconFactory;
import de.rgse.timecap.view.util.IconGroesseEnum;

public class ChartPane extends FrameTemplate {

    private static final long serialVersionUID = -3775675446311979576L;

    protected ChartPane(List<Arbeitstag> days) throws NumberFormatException, ParseException {
        super("Verlauf im " + ChartPane.getMonth(days), IconFactory.getChart(IconGroesseEnum.PX32));
        setContentPane(createContentPane(days));
        pack();
        setVisible(true);
    }

    private JXPanel createContentPane(List<Arbeitstag> days) throws NumberFormatException, ParseException {
        ChartPanel chartPanel = new ChartPanel(new ChartService().createLineChart(days,
                "Verlauf im " + ChartPane.getMonth(days)));

        JXButton closeBtn = new JXButton("Schliessen");
        FrameTemplate.initComponent(closeBtn);
        closeBtn.setIcon(IconFactory.getCancle(IconGroesseEnum.PX16));
        closeBtn.addActionListener(getBackAction());

        JXPanel controllPanel = new JXPanel(new FlowLayout(FlowLayout.RIGHT));
        FrameTemplate.initComponent(controllPanel);
        controllPanel.add(closeBtn);

        JXPanel contentPanel = new JXPanel(new BorderLayout());
        FrameTemplate.initComponent(contentPanel);
        contentPanel.add(BorderLayout.CENTER, chartPanel);
        contentPanel.add(BorderLayout.SOUTH, controllPanel);

        controllPanel.setBorder(BorderFactory.createEmptyBorder(20, 20, 20, 20));

        return contentPanel;
    }

    private ActionListener getBackAction() {
        return new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                new UebersichtArbeitstagePage();
                dispose();
            }
        };
    }

    private static String getMonth(List<Arbeitstag> days) {
        return days.get(0).getDatum().getMonatsname();
    }

}
