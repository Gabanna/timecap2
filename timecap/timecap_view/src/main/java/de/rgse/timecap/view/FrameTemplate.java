package de.rgse.timecap.view;

import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.event.ActionEvent;

import javax.swing.AbstractAction;
import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.JComponent;
import javax.swing.JFrame;
import javax.swing.KeyStroke;
import javax.swing.WindowConstants;

import org.jdesktop.swingx.JXFrame;
import org.jdesktop.swingx.JXTitledPanel;

import de.rgse.timecap.view.util.IconFactory;
import de.rgse.timecap.view.util.ViewUtil;

/**
 */
public abstract class FrameTemplate extends JXFrame {

    private static final int HEIGHT = 370;

    private static final Dimension DIMENSION = new Dimension((int) (2.5 * FrameTemplate.HEIGHT), FrameTemplate.HEIGHT);

    private static final long serialVersionUID = -71558607955683048L;

    protected FrameTemplate(String title, ImageIcon icon) {
        setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
        setTitle(title);
        setSize(FrameTemplate.DIMENSION);
        setPreferredSize(FrameTemplate.DIMENSION);
        setLocationRelativeTo(null);
    }

    protected FrameTemplate() {
        super();
        setIconImage(IconFactory.getLogoIcon().getImage());
        setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
        setLocationRelativeTo(null);
    }

    public static void initComponent(JComponent component) {
        component.setBackground(Color.WHITE);
        FrameTemplate.setGlobalKeyListener(component);

        if (component instanceof JXTitledPanel) {
            JXTitledPanel panel = (JXTitledPanel) component;
            panel.setTitlePainter(ViewUtil.getTitlePainter());
            panel.setBorder(BorderFactory.createLineBorder(ViewUtil.getGreencolor()));
        }
    }

    private static void setGlobalKeyListener(final JComponent component) {
        component.getInputMap().put(KeyStroke.getKeyStroke("ESCAPE"), "escapeAction");
        component.getActionMap().put("escapeAction", new AbstractAction() {

            private static final long serialVersionUID = 5558825362356537438L;

            @Override
            public void actionPerformed(ActionEvent e) {
                Component comp = component;
                while (!(comp instanceof JFrame)) {
                    comp = comp.getParent();
                }
                ((JFrame) comp).dispose();
            }
        });
    }
}
