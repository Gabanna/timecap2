package de.rgse.timecap.reporting;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.ParseException;
import java.util.Collections;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Locale;

import org.apache.log4j.Logger;
import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFCreationHelper;
import org.apache.poi.hssf.usermodel.HSSFFormulaEvaluator;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;

import de.rgse.timecap.model.Arbeitstag;
import de.rgse.timecap.model.ReportObjekt;
import de.rgse.timecap.model.util.DayComparator;
import de.rgse.timecap.model.util.UhrzeitUtil;
import de.rgse.timecap.service.ServiceProvider;
import de.rgse.timecap.service.TagAnalyseService;

public class ReporterService {
	private static final Logger LOGGER = Logger
			.getLogger(ReporterService.class);
	private final TagAnalyseService tagAnalyseService = ServiceProvider
			.getTagAnalyseServiceInstance();

	public static final int ROW_TITLE = 0;
	public static final int ROW_OVERLAY = 7;
	public static final int ROW_FIRST_OF_MONTH = 9;
	public static final int ROW_SUM_WORKINGTIME = 39;
	public static final int COLUMN_TITLE = 1;
	public static final int COLUMN_DAYNAME = 1;
	public static final int COLUMN_DATE = 2;
	public static final int COLUMN_PRESENT_FROM = 3;
	public static final int COLUMN_PRESENT_UNTIL = 4;
	public static final int COLUMN_BREAK_FROM = 5;
	public static final int COLUMN_BREAK_UNTIL = 6;
	public static final int COLUMN_DIFFERENCE = 7;
	public static final int COLUMN_WORKINGTIME = 9;

	private void evaluteFormulaCells(HSSFFormulaEvaluator formulaEvaluator,
			HSSFSheet sheet) {
		int i = 8;
		HSSFCell cell;
		while (null != sheet.getRow(i)
				&& null != (cell = sheet.getRow(i++).getCell(11))) {
			formulaEvaluator.evaluateFormulaCell(cell);
		}
		formulaEvaluator.evaluateFormulaCell(sheet.getRow(
				ReporterService.ROW_SUM_WORKINGTIME).getCell(
				ReporterService.COLUMN_WORKINGTIME));
		formulaEvaluator.evaluateFormulaCell(sheet.getRow(
				ReporterService.ROW_OVERLAY).getCell(
				ReporterService.COLUMN_WORKINGTIME));
	}

	private int getDayOfMonth(Arbeitstag day) {
		return day.getDatum().get(5);
	}

	private String getMonthAsString(Arbeitstag day) {
		return day.getDatum().getDisplayName(2, 1, Locale.GERMANY);
	}

	public void reportOfficialDays(String destPath) throws IOException,
			ParseException {
		List<Arbeitstag> arbeitstage = tagAnalyseService
				.findAllByYear(new GregorianCalendar(Locale.GERMANY));
		Collections.sort(arbeitstage, new DayComparator());
		try {
			int row = ReporterService.ROW_FIRST_OF_MONTH;
			HSSFWorkbook workbook = new HSSFWorkbook(new FileInputStream(
					destPath));
			HSSFCreationHelper creationHelper = workbook.getCreationHelper();
			HSSFFormulaEvaluator formulaEvaluator = creationHelper
					.createFormulaEvaluator();
			HSSFSheet sheet = null;

			for (Arbeitstag arbeitstag : arbeitstage) {
				ReportObjekt reportObjekt = tagAnalyseService
						.findReportObjektByArbeitstag(arbeitstag);
				sheet = workbook.getSheet(getMonthAsString(arbeitstag));
				row = ReporterService.ROW_FIRST_OF_MONTH
						+ getDayOfMonth(arbeitstag) - 2;

				sheet.getRow(row)
						.getCell(ReporterService.COLUMN_PRESENT_FROM)
						.setCellValue(
								UhrzeitUtil.parseStringToCalendar(reportObjekt
										.getTagesStart()));
				if (null != reportObjekt.getTagesEnde()) {
					sheet.getRow(row)
							.getCell(ReporterService.COLUMN_PRESENT_UNTIL)
							.setCellValue(
									UhrzeitUtil
											.parseStringToCalendar(reportObjekt
													.getTagesEnde()));
				}
				if (null != reportObjekt.getPause()) {
					sheet.getRow(row)
							.getCell(ReporterService.COLUMN_BREAK_FROM)
							.setCellValue(
									UhrzeitUtil
											.parseStringToCalendar(reportObjekt
													.getPause().getStart()));
					if (null != reportObjekt.getPause().getEnde()) {
						sheet.getRow(row)
								.getCell(ReporterService.COLUMN_BREAK_UNTIL)
								.setCellValue(
										UhrzeitUtil
												.parseStringToCalendar(reportObjekt
														.getPause().getEnde()));
					}
				}
				formulaEvaluator.evaluateFormulaCell(sheet.getRow(row).getCell(
						ReporterService.COLUMN_WORKINGTIME));
			}
			evaluteFormulaCells(formulaEvaluator, sheet);

			FileOutputStream outputStream = new FileOutputStream(destPath);
			workbook.write(outputStream);
			outputStream.close();
		} catch (IOException e) {
			ReporterService.LOGGER.error(e);
			throw e;
		} catch (NumberFormatException e) {
			ReporterService.LOGGER.error(e);
			throw e;
		} catch (ParseException e) {
			ReporterService.LOGGER.error(e);
			throw e;
		}
	}
}