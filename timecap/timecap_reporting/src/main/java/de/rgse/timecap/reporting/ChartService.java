package de.rgse.timecap.reporting;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.data.category.CategoryDataset;
import org.jfree.data.category.DefaultCategoryDataset;

import de.rgse.timecap.model.Arbeitstag;
import de.rgse.timecap.model.util.DayComparator;
import de.rgse.timecap.model.util.UhrzeitUtil;

public class ChartService {

    public ChartPanel createBarChart(String title, String nameXaxis, String nameYaxis, List<Arbeitstag> days)
            throws NumberFormatException, ParseException {
        JFreeChart chart = ChartFactory.createBarChart(title, nameXaxis, nameYaxis, createDataSet(days),
                PlotOrientation.VERTICAL, true, true, false);

        return new ChartPanel(chart);
    }

    private CategoryDataset createDataSet(List<Arbeitstag> days) throws NumberFormatException, ParseException {
        Collections.sort(days, new DayComparator());

        Calendar cal = days.get(0).getDatum().asCalendar();

        cal.set(5, 1);
        Date firstDate = new Date(cal.getTimeInMillis());

        cal.set(5, cal.getActualMaximum(5) + 1);
        Date lastDate = new Date(cal.getTimeInMillis());

        String lineName = "Arbeitszeit";
        int index = 0;
        SimpleDateFormat formatter = new SimpleDateFormat("dd.");

        DefaultCategoryDataset dataSet = new DefaultCategoryDataset();
        while (firstDate.before(lastDate)) {
            double value = 0.0D;
            if (index < days.size()) {
                Arbeitstag day = days.get(index);
                if (day.getDatum().asCalendar().getTime().equals(firstDate)) {
                    index++;
                    if (null != day.getTagesEnde()) {
                        String differenz = UhrzeitUtil.getDifferenz(day.getTagesEnde(), day.getTagesStart());
                        String[] splits = differenz.split(":");
                        value = Integer.valueOf(splits[0]).intValue() + Integer.valueOf(splits[1]).intValue() / 100;
                    }
                }
            }
            dataSet.addValue(value, lineName, formatter.format(firstDate));
            firstDate.setTime(firstDate.getTime() + 86400000L);
        }
        return dataSet;
    }

    public CategoryDataset createEmptyDataSet(String rowKey, String columnKey) {
        DefaultCategoryDataset set = new DefaultCategoryDataset();
        set.addValue(0.0D, rowKey, columnKey);
        return set;
    }

    public JFreeChart createLineChart(List<Arbeitstag> days, String title) throws NumberFormatException, ParseException {
        CategoryDataset dataSet;
        if (days.isEmpty()) {
            dataSet = createEmptyDataSet("", "keine Daten");
        } else {
            dataSet = createDataSet(days);
        }
        JFreeChart chart = ChartFactory.createLineChart(title, "Tage", "Arbeitszeit", dataSet,
                PlotOrientation.VERTICAL, true, true, false);

        return chart;
    }
}
