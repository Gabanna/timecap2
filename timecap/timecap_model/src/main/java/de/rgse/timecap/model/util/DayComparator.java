package de.rgse.timecap.model.util;

import java.util.Comparator;

import de.rgse.timecap.model.Arbeitstag;

public class DayComparator implements Comparator<Arbeitstag> {

    @Override
    public int compare(Arbeitstag o1, Arbeitstag o2) {
        return o1.getDatum().compareTo(o2.getDatum());
    }

}
