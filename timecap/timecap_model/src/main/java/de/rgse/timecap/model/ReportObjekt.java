package de.rgse.timecap.model;

import java.io.Serializable;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.Transient;

@Entity
public class ReportObjekt implements Serializable {

    @Id
    @GeneratedValue
    private long id;

    private String tagesStart;

    private String tagesEnde;

    @OneToOne
    private Arbeitstag arbeitstag;

    @OneToOne(cascade = CascadeType.ALL)
    private Pause pause;

    @Transient
    private static final long serialVersionUID = -2060591628603994273L;

    protected ReportObjekt() {
    }

    public ReportObjekt(Arbeitstag arbeitstag, String tagesStart) {
        this.arbeitstag = arbeitstag;
        this.tagesStart = tagesStart;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getTagesEnde() {
        return tagesEnde;
    }

    public void setTagesEnde(String tagesEnde) {
        this.tagesEnde = tagesEnde;
    }

    public String getTagesStart() {
        return tagesStart;
    }

    public void setTagesStart(String tagesStart) {
        this.tagesStart = tagesStart;
    }

    public Arbeitstag getArbeitstag() {
        return arbeitstag;
    }

    public void setArbeitstag(Arbeitstag arbeitstag) {
        this.arbeitstag = arbeitstag;
    }

    public Pause getPause() {
        return pause;
    }

    public void setPausen(Pause pause) {
        this.pause = pause;
    }

    @Override
    public String toString() {
        return "ReportObject@" + getArbeitstag().getDatumAsString();
    }
}
