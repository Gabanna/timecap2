package de.rgse.timecap.exceptions;

public class ArbeitszeitUeberschreitungsException extends Exception {

    private static final long serialVersionUID = 8193655514436407470L;

    public ArbeitszeitUeberschreitungsException() {
        super();
    }

    public ArbeitszeitUeberschreitungsException(String message) {
        super(message);
    }

    public ArbeitszeitUeberschreitungsException(Throwable cause) {
        super(cause);
    }

    public ArbeitszeitUeberschreitungsException(String message, Throwable cause) {
        super(message, cause);
    }

    public ArbeitszeitUeberschreitungsException(String message, Throwable cause, boolean enableSuppression,
            boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
