package de.rgse.timecap.model.util;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.apache.log4j.FileAppender;
import org.apache.log4j.Layout;
import org.apache.log4j.spi.ErrorCode;

/**
 * This is a customized log4j appender, which will create a new file for every
 * run of the application.
 * 
 * @author veera | http://veerasundar.com
 * 
 * @version $Revision: 1.0 $
 */
public class MonthlyFileAppender extends FileAppender {

    public MonthlyFileAppender() {
    }

    /**
     * Constructor for MonthlyFileAppender.
     * 
     * @param layout
     *            Layout
     * @param filename
     *            String
     * @throws IOException
     */
    public MonthlyFileAppender(Layout layout, String filename) throws IOException {
        super(layout, filename);
    }

    /**
     * Constructor for MonthlyFileAppender.
     * 
     * @param layout
     *            Layout
     * @param filename
     *            String
     * @param append
     *            boolean
     * @throws IOException
     */
    public MonthlyFileAppender(Layout layout, String filename, boolean append) throws IOException {
        super(layout, filename, append);
    }

    /**
     * Constructor for MonthlyFileAppender.
     * 
     * @param layout
     *            Layout
     * @param filename
     *            String
     * @param append
     *            boolean
     * @param bufferedIO
     *            boolean
     * @param bufferSize
     *            int
     * @throws IOException
     */
    public MonthlyFileAppender(Layout layout, String filename, boolean append, boolean bufferedIO, int bufferSize)
            throws IOException {
        super(layout, filename, append, bufferedIO, bufferSize);
    }

    /**
     * Method activateOptions.
     * 
     * @see org.apache.log4j.spi.OptionHandler#activateOptions()
     */
    @Override
    public void activateOptions() {
        if (fileName != null) {
            try {
                String date = new SimpleDateFormat("MM_yy").format(new Date()); //$NON-NLS-1$

                fileName = "resource/logs/log_" + date + ".log"; //$NON-NLS-1$ //$NON-NLS-2$
                setFile(fileName, fileAppend, bufferedIO, bufferSize);
            } catch (Exception e) {
                errorHandler.error("Error while activating log options", e, ErrorCode.FILE_OPEN_FAILURE); //$NON-NLS-1$
            }
        }
    }
}