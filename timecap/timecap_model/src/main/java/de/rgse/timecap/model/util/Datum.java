package de.rgse.timecap.model.util;

import java.io.Serializable;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.Locale;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Transient;

@Entity
public class Datum implements Serializable, Comparable<Datum> {

    @Transient
    private static final long serialVersionUID = 1L;

    @Id
    private int tag;

    @Id
    private int monat;

    @Id
    private int jahr;

    @Transient
    private static final String[] MONATSNAMEN = { "Jan", "Feb", "Mrz", "Apr", "Mai", "Jun", "Jul", "Aug", "Sep", "Okt",
            "Nov", "Dez" };

    private String wochentag;

    private Datum(String wochentag, int tag, int monat, int jahr) {
        this.wochentag = wochentag;
        this.tag = tag;
        this.monat = monat;
        this.jahr = jahr;
    }

    protected Datum() {

    }

    public Datum(Calendar calendar) {
        setDatum(calendar);
    }

    public int getTag() {
        return tag;
    }

    public int getMonat() {
        return monat;
    }

    public int getJahr() {
        return jahr;
    }

    public String getWochentag() {
        return wochentag;
    }

    protected void setWochentag(String wochentag) {
        this.wochentag = wochentag;
    }

    protected void setTag(int tag) {
        this.tag = tag;
    }

    protected void setMonat(int monat) {
        this.monat = monat;
    }

    protected void setJahr(int jahr) {
        this.jahr = jahr;
    }

    public void setDatum(Calendar calendar) {
        tag = calendar.get(Calendar.DATE);
        monat = calendar.get(Calendar.MONTH)+1;
        jahr = calendar.get(Calendar.YEAR);
        wochentag = calendar.getDisplayName(Calendar.DAY_OF_WEEK, Calendar.SHORT, Locale.GERMANY);
    }

    @Override
    public String toString() {
        return wochentag + " " + tag + "." + monat + "." + jahr;
    }

    @Override
    public int compareTo(Datum o) {
        int result = Integer.valueOf(jahr).compareTo(Integer.valueOf(o.jahr));

        if (0 == result) {
            result = Integer.valueOf(monat).compareTo(Integer.valueOf(o.monat));
        }

        if (0 == result) {
            result = Integer.valueOf(tag).compareTo(Integer.valueOf(o.tag));
        }

        return result;
    }

    public Calendar asCalendar() {
        return new GregorianCalendar(jahr, monat-1, tag);
    }

    public Datum duplicate() {
        return new Datum(wochentag, tag, monat, jahr);
    }

    @Override
    public boolean equals(Object obj) {
        boolean result = false;

        if (obj instanceof Datum) {
            Datum datum = (Datum) obj;
            result = tag == datum.tag && monat == datum.monat && jahr == datum.jahr;
        }

        return result;
    }

    public String getMonatsname() {
        return Datum.MONATSNAMEN[monat];
    }
}
