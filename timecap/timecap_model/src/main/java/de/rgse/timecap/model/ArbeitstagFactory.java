package de.rgse.timecap.model;

import java.text.ParseException;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.Locale;

import de.rgse.timecap.exceptions.PausenlimitErreichtException;
import de.rgse.timecap.model.util.UhrzeitUtil;

public class ArbeitstagFactory {

    public void setTagesStart(Arbeitstag arbeitstag, String tagesStart) {
        if (UhrzeitUtil.uhrzeitValidieren(tagesStart)) {
            arbeitstag.setTagesStart(tagesStart);
        } else {
            throw new IllegalArgumentException("ungültiges Zeitformat: '" + "'");
        }
    }

    public Arbeitstag createArbeitstag(Calendar datum) {
        return new Arbeitstag(datum, UhrzeitUtil.format(datum));
    }

    public void setTagesEnde(Arbeitstag arbeitstag, String tagesEnde) {
        if (UhrzeitUtil.uhrzeitValidieren(tagesEnde)) {
            arbeitstag.setTagesEnde(tagesEnde);
        } else {
            throw new IllegalArgumentException("ungültiges Zeitformat: '" + tagesEnde + "'");
        }
    }

    public void pauseEintragen(Arbeitstag arbeitstag, Calendar calendar) throws NumberFormatException, ParseException {
        Pause pause = new Pause(arbeitstag, UhrzeitUtil.format(calendar));
        arbeitstag.setPause(pause);
    }

    public void pauseEintragen(Arbeitstag arbeitstag) throws PausenlimitErreichtException, NumberFormatException,
            ParseException {
        pauseEintragen(arbeitstag, new GregorianCalendar(Locale.GERMANY));
    }

    public ReportObjekt erstelleVertretungstag(Calendar calendar) {
        Arbeitstag arbeitstag = new Arbeitstag(calendar, null);
        arbeitstag.setVertretungstag(true);
        ReportObjekt reportObjekt = new ReportObjekt(arbeitstag, "09:00");
        reportObjekt.setTagesEnde("19:00");

        return reportObjekt;
    }
}
