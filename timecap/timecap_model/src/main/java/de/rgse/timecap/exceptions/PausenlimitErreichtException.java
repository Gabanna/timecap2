package de.rgse.timecap.exceptions;

public class PausenlimitErreichtException extends ArrayIndexOutOfBoundsException {

    private static final long serialVersionUID = 134130961178704453L;

    public PausenlimitErreichtException() {
        super();
    }

    public PausenlimitErreichtException(String message) {
        super(message);
    }

    public PausenlimitErreichtException(int arg0) {
        super(arg0);
    }
}
