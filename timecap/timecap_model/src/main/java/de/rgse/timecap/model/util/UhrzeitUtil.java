package de.rgse.timecap.model.util;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Locale;

import org.apache.log4j.Logger;

public class UhrzeitUtil {

    private static final DateFormat dateFormat = new SimpleDateFormat("kk:mm"); //$NON-NLS-1$

    private static final Logger LOGGER = Logger.getLogger(UhrzeitUtil.class);

    public static int compare(String zeit, String otherZeit) {
	int result = 0;
        try {
	    Date date = UhrzeitUtil.dateFormat.parse(zeit);
	    Date otherDate = UhrzeitUtil.dateFormat.parse(otherZeit);
	    result = date.compareTo(otherDate);
        } catch (ParseException e) {
	    UhrzeitUtil.LOGGER.warn(e);
	}
	return result;
    }

    public static Calendar parseStringToCalendar(String uhrzeit) throws ParseException, NumberFormatException
    {

        if (UhrzeitUtil.uhrzeitValidieren(uhrzeit)) {
	    Calendar result = new GregorianCalendar(Locale.GERMANY);
	    result.setTime(UhrzeitUtil.dateFormat.parse(uhrzeit));

	    return result;

        } else {
	    throw new NumberFormatException("ungültiges Zeitformat: '" + uhrzeit + "'"); //$NON-NLS-1$ //$NON-NLS-2$
	}
    }

    public static boolean uhrzeitValidieren(String uhrzeit) {
	boolean result = true;
        try {
	    String[] splits = uhrzeit.split(":"); //$NON-NLS-1$
	    int stunde = Integer.valueOf(splits[0]);
	    int minute = Integer.valueOf(splits[1]);

            if (stunde >= 24 && stunde < 48) {
		stunde -= 24;
	    }
            if (minute >= 60 && minute < 120) {
		minute -= 60;
	    }
            if (stunde < 0 || minute < 0) {
		result = false;
	    }
        } catch (NumberFormatException e) {
	    result = false;
        } catch (IndexOutOfBoundsException e) {
	    result = false;
	}
	return result;
    }

    public static String format(Calendar datum) {
	return UhrzeitUtil.dateFormat.format(((Calendar) datum.clone()).getTime());
    }

    public static String getDifferenz(String zeit1, String zeit2) throws NumberFormatException, ParseException {
        if (!UhrzeitUtil.uhrzeitValidieren(zeit1) || !UhrzeitUtil.uhrzeitValidieren(zeit2)) {
	    throw new IllegalArgumentException();
	}
	long delta = UhrzeitUtil.parseStringToCalendar(zeit1).getTimeInMillis()
		- UhrzeitUtil.parseStringToCalendar(zeit2).getTimeInMillis();
	Calendar calendar = new GregorianCalendar(Locale.GERMANY);
	calendar.setTimeInMillis(delta);
	return UhrzeitUtil.format(calendar);
    }

    public static String uhrzeitVariieren(String zeit) throws NumberFormatException, ParseException {
        if (!UhrzeitUtil.uhrzeitValidieren(zeit)) {
	    throw new IllegalArgumentException(zeit);
	}

	GregorianCalendar calendar = (GregorianCalendar) UhrzeitUtil.parseStringToCalendar(zeit);
	int minuten = calendar.get(Calendar.MINUTE);
	minuten = minuten + (int) (Math.random() * 30 - 120);

	calendar.set(Calendar.MINUTE, minuten);

	return UhrzeitUtil.format(calendar);
    }
}
