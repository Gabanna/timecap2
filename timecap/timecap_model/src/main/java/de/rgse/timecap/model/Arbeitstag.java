package de.rgse.timecap.model;

import java.io.Serializable;
import java.util.Calendar;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.Transient;

import de.rgse.timecap.model.util.Datum;

@Entity
public class Arbeitstag implements Serializable {

    @Id
    @GeneratedValue
    private long id;

    @OneToOne(cascade = CascadeType.ALL)
    private Datum datum;

    private String tagesStart;

    private String tagesEnde;

    @OneToOne(cascade = CascadeType.ALL)
    private Pause pause;

    private boolean vertretungstag;

    @Transient
    private static final long serialVersionUID = -6219983740877512066L;

    protected Arbeitstag() {
    }

    /**
     * Use de.rgse.service.Zeitservice to create Arbeitstag-instance
     * 
     * @param datum
     */
    protected Arbeitstag(Calendar datum, String tagesStart) {
        this.datum = new Datum(datum);
        this.tagesStart = tagesStart;
    }

    public void setId(long id) {
        this.id = id;
    }

    protected void setDatum(Calendar datum) {
        this.datum.setDatum(datum);
    }

    public void setTagesStart(String tagesStart) {
        this.tagesStart = tagesStart;
    }

    public void setTagesEnde(String tagesEnde) {
        this.tagesEnde = tagesEnde;
    }

    public long getId() {
        return id;
    }

    public Datum getDatum() {
        return datum;
    }

    public String getDatumAsString() {
        return datum.toString();
    }

    public String getTagesStart() {
        return tagesStart;
    }

    public String getTagesEnde() {
        return tagesEnde;
    }

    public Pause getPause() {
        return pause;
    }

    public void setPause(Pause pause) {
        this.pause = pause;
    }

    public boolean isVertretungstag() {
        return vertretungstag;
    }

    public void setVertretungstag(boolean vertretungstag) {
        this.vertretungstag = vertretungstag;
    }

    @Override
    public String toString() {
        return "Arbeitstag@" + getDatumAsString();
    }
}
