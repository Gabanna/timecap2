package de.rgse.timecap.model;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Transient;

@Entity
public class Pause implements Serializable {

    @Id
    @GeneratedValue
    private long id;

    private String start;

    private String ende;

    @ManyToOne
    private Arbeitstag arbeitstag;

    @Transient
    private static final long serialVersionUID = -8533524144549476544L;

    protected Pause() {
    }

    public Pause(Arbeitstag arbeitstag, String start) {
        this.start = start;
        this.arbeitstag = arbeitstag;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getStart() {
        return start;
    }

    public void setStart(String start) {
        this.start = start;
    }

    public String getEnde() {
        return ende;
    }

    public void setEnde(String ende) {
        this.ende = ende;
    }

    public Arbeitstag getArbeitstag() {
        return arbeitstag;
    }

    public void setArbeitstag(Arbeitstag arbeitstag) {
        this.arbeitstag = arbeitstag;
    }
}
