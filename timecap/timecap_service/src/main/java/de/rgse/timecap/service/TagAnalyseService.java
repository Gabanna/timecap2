package de.rgse.timecap.service;

import java.util.Calendar;
import java.util.List;

import de.rgse.timecap.model.Arbeitstag;
import de.rgse.timecap.model.ReportObjekt;
import de.rgse.timecap.model.util.UhrzeitUtil;
import de.rgse.timecap.persistence.ArbeitstagDAO;
import de.rgse.timecap.persistence.PersistenceProvider;
import de.rgse.timecap.persistence.ReportObjektDAO;

public class TagAnalyseService {

	private final ArbeitstagDAO arbeitstagDAO = PersistenceProvider
			.getArbeitstagDAOInstance();
	private final ReportObjektDAO reportObjektDAO = PersistenceProvider
			.getReportObjektDAOInstance();

	TagAnalyseService() {
	}

	public Arbeitstag findBy(Calendar calendar) {
		return arbeitstagDAO.findByDatum(UhrzeitUtil.formatToDate(calendar));
	}

	public Arbeitstag findBy(long id) {
		return arbeitstagDAO.findById(id);
	}

	public List<Arbeitstag> findAllByMonth(Calendar calendar) {
		return arbeitstagDAO.findAllByMonth(UhrzeitUtil.formatToDate(calendar));
	}

	public List<Arbeitstag> findAllByYear(Calendar calendar) {
		return arbeitstagDAO.findAllByYear(UhrzeitUtil.formatToDate(calendar));
	}

	public void update(Arbeitstag arbeitstag) {
		arbeitstagDAO.merge(arbeitstag);
	}

	public ReportObjekt findReportObjektByArbeitstag(Arbeitstag arbeitstag) {
		return reportObjektDAO.findByArbeitstag(arbeitstag);
	}

	public void update(ReportObjekt reportObjekt) {
		reportObjektDAO.merge(reportObjekt);
	}

	public void removeDay(long id) throws Exception {
		Arbeitstag arbeitstag = findBy(id);
		ReportObjekt reportObjekt = reportObjektDAO
				.findByArbeitstag(arbeitstag);
		reportObjektDAO.delete(reportObjekt);
		arbeitstagDAO.delete(arbeitstag);
	}

}
