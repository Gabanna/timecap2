package de.rgse.timecap.exceptions;

import de.rgse.timecap.model.ReportObjekt;

public class PotetielleKorrekturException extends Exception {

    private static final long serialVersionUID = 1L;

    private ReportObjekt reportObjekt;

    public PotetielleKorrekturException(ReportObjekt reportObjekt) {
        this.reportObjekt = reportObjekt;
    }

    public ReportObjekt getReportObjekt() {
        return reportObjekt;
    }
}
