package de.rgse.timecap.exceptions;

import javax.persistence.EntityExistsException;

public class ArbeitstagExistiertException extends EntityExistsException {

    private static final long serialVersionUID = -8205969658512932951L;

    public ArbeitstagExistiertException() {
        super();
    }

    public ArbeitstagExistiertException(String message) {
        super(message);
    }

    public ArbeitstagExistiertException(Throwable cause) {
        super(cause);
    }

    public ArbeitstagExistiertException(String message, Throwable cause) {
        super(message, cause);
    }
}
