package de.rgse.timecap.service;

import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.Locale;

import de.rgse.timecap.exceptions.ArbeitstagExistiertException;
import de.rgse.timecap.exceptions.PotetielleKorrekturException;
import de.rgse.timecap.model.Arbeitstag;
import de.rgse.timecap.model.ArbeitstagFactory;
import de.rgse.timecap.model.ReportObjekt;
import de.rgse.timecap.model.util.UhrzeitUtil;
import de.rgse.timecap.persistence.ArbeitstagDAO;
import de.rgse.timecap.persistence.PersistenceProvider;
import de.rgse.timecap.persistence.ReportObjektDAO;

public class TagBeginnenService {

    private final ArbeitstagDAO arbeitstagDAO = PersistenceProvider.getArbeitstagDAOInstance();

    private final ReportObjektDAO reportObjektDAO = PersistenceProvider.getReportObjektDAOInstance();

    private final ArbeitstagFactory arbeitstagFactory = PersistenceProvider.getArbeitstagFactoryInstance();

    TagBeginnenService() {
    }

    public void erstelleVertretungstag(Calendar calendar) {
        ReportObjekt reportObjekt = new ArbeitstagFactory().erstelleVertretungstag(calendar);
        arbeitstagDAO.persist(reportObjekt.getArbeitstag());
        reportObjektDAO.persist(reportObjekt);
    }

    public void beginneTag() throws ArbeitstagExistiertException {
        GregorianCalendar gregorianCalendar = new GregorianCalendar(Locale.GERMANY);
        Arbeitstag arbeitstag = arbeitstagDAO.findByDatum(gregorianCalendar);

        if (null == arbeitstag) {
            arbeitstag = arbeitstagFactory.createArbeitstag(gregorianCalendar);
            ReportObjekt reportObjekt = new ReportObjekt(arbeitstag, startZeitRunden(arbeitstag));
            arbeitstagDAO.persist(arbeitstag);
            reportObjektDAO.persist(reportObjekt);
        } else if (arbeitstag.isVertretungstag()) {
            arbeitstag.setTagesStart(UhrzeitUtil.format(gregorianCalendar));
            arbeitstagDAO.merge(arbeitstag);
        } else {
            throw new ArbeitstagExistiertException();
        }
    }

    public void letztenTagAufUeberlaengeTesten() throws PotetielleKorrekturException {
        Calendar calendar = new GregorianCalendar(Locale.GERMANY);
        String name = calendar.getDisplayName(Calendar.DAY_OF_WEEK, Calendar.SHORT, Locale.GERMANY);
        if (!name.equals("Mo")) {
            calendar.set(Calendar.DAY_OF_MONTH, calendar.get(Calendar.DAY_OF_MONTH) - 1);
            ReportObjekt letztesReportObjekt = reportObjektDAO.findByDatum(calendar);

            if (null != letztesReportObjekt && letztesReportObjekt.getTagesStart().equals("09:00")
                    && letztesReportObjekt.getTagesEnde().equals("19:00")) {
                throw new PotetielleKorrekturException(letztesReportObjekt);
            }
        }
    }

    private String startZeitRunden(Arbeitstag arbeitstag) {
        String result;
        int vergleich = UhrzeitUtil.compare(arbeitstag.getTagesStart(), "9:30");

        if (vergleich < 0) {
            result = "9:00";

        } else {
            result = "10:00";
        }
        return result;
    }

    public void tagUeberschreiben() throws Exception {
        Arbeitstag arbeitstag = arbeitstagDAO.findByDatum(new GregorianCalendar(Locale.GERMANY));
        arbeitstagFactory.setTagesStart(arbeitstag, UhrzeitUtil.format(new GregorianCalendar(Locale.GERMANY)));
        arbeitstagDAO.merge(arbeitstag);
    }

    public Arbeitstag erstelleTag(GregorianCalendar date) {
        Arbeitstag arbeitstag = arbeitstagDAO.findByDatum(date);

        if (null == arbeitstag) {
            arbeitstag = arbeitstagFactory.createArbeitstag(date);
            ReportObjekt reportObjekt = new ReportObjekt(arbeitstag, startZeitRunden(arbeitstag));
            arbeitstagDAO.persist(arbeitstag);
            reportObjektDAO.persist(reportObjekt);
        } else if (arbeitstag.isVertretungstag()) {
            arbeitstag.setTagesStart(UhrzeitUtil.format(date));
            arbeitstagDAO.merge(arbeitstag);
        } else {
            throw new ArbeitstagExistiertException();
        }

        return arbeitstag;
    }

    public void reportObjectBeginnen(ReportObjekt reportObjekt, String string) {
        reportObjekt.setTagesStart(string);
        reportObjektDAO.merge(reportObjekt);
    }
}
