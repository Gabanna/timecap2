package de.rgse.timecap.service;

import java.text.ParseException;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.Locale;

import de.rgse.timecap.exceptions.PausenlimitErreichtException;
import de.rgse.timecap.model.Arbeitstag;
import de.rgse.timecap.model.ArbeitstagFactory;
import de.rgse.timecap.persistence.ArbeitstagDAO;
import de.rgse.timecap.persistence.PersistenceProvider;

public class PausenService {

    private ArbeitstagDAO arbeitstagDAO = PersistenceProvider.getArbeitstagDAOInstance();

    private ArbeitstagFactory arbeitstagFactory = PersistenceProvider.getArbeitstagFactoryInstance();

    PausenService() {
    }

    public void pauseEintragen() throws PausenlimitErreichtException, NumberFormatException, ParseException {
        pauseEintragen(new GregorianCalendar(Locale.GERMANY));
    }

    public void pauseEintragen(Calendar calendar) throws NumberFormatException, ParseException {
        Arbeitstag arbeitstag = arbeitstagDAO.findByDatum(calendar);
        arbeitstagFactory.pauseEintragen(arbeitstag, calendar);
        arbeitstagDAO.merge(arbeitstag);
    }
}
