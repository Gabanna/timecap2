package de.rgse.timecap.service;

import java.text.ParseException;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.Locale;

import org.apache.log4j.Logger;

import de.rgse.timecap.exceptions.ArbeitszeitUeberschreitungsException;
import de.rgse.timecap.model.Arbeitstag;
import de.rgse.timecap.model.ArbeitstagFactory;
import de.rgse.timecap.model.Pause;
import de.rgse.timecap.model.ReportObjekt;
import de.rgse.timecap.model.util.UhrzeitUtil;
import de.rgse.timecap.persistence.ArbeitstagDAO;
import de.rgse.timecap.persistence.PersistenceProvider;
import de.rgse.timecap.persistence.ReportObjektDAO;

public class TagBeendenService {

    private final ReportObjektDAO reportObjektDAO = PersistenceProvider.getReportObjektDAOInstance();

    private final ArbeitstagDAO arbeitstagDAO = PersistenceProvider.getArbeitstagDAOInstance();

    private final ArbeitstagFactory arbeitstagFactory = PersistenceProvider.getArbeitstagFactoryInstance();

    private static final Logger LOGGER = Logger.getLogger(TagBeendenService.class);

    TagBeendenService() {
    }

    public void tagBeenden() throws ArbeitszeitUeberschreitungsException {
        GregorianCalendar gregorianCalendar = new GregorianCalendar(Locale.GERMANY);

        Arbeitstag arbeitstag = arbeitstagDAO.findByDatum(gregorianCalendar);
        TagBeendenService.LOGGER.info("arbeitstag = " + arbeitstag);
        arbeitstagFactory.setTagesEnde(arbeitstag, UhrzeitUtil.format(gregorianCalendar));
        TagBeendenService.LOGGER.info("setTagesEnde: " + UhrzeitUtil.format(gregorianCalendar));
        arbeitstagDAO.merge(arbeitstag);

        TagBeendenService.LOGGER
                .info("!arbeitstag.isVertretungstag() && (UhrzeitUtil.compare(arbeitstag.getTagesStart(), \"09:15\") == -1) && UhrzeitUtil.compare(arbeitstag.getTagesEnde(), \"18:15\") == 1 = "
                        + (!arbeitstag.isVertretungstag()
                                && UhrzeitUtil.compare(arbeitstag.getTagesStart(), "09:15") == -1 && UhrzeitUtil
                                .compare(arbeitstag.getTagesEnde(), "18:15") == 1));
        if (!arbeitstag.isVertretungstag() && UhrzeitUtil.compare(arbeitstag.getTagesStart(), "09:15") == -1
                && UhrzeitUtil.compare(arbeitstag.getTagesEnde(), "18:15") == 1) {
            throw new ArbeitszeitUeberschreitungsException();
        }

        ReportObjekt reportObjekt = reportObjektDAO.findByArbeitstag(arbeitstag);
        TagBeendenService.LOGGER.info("reportObject = " + reportObjekt);

        TagBeendenService.LOGGER.info("!arbeitstag.isVertretungstag() = " + !arbeitstag.isVertretungstag());
        if (!arbeitstag.isVertretungstag()) {
            tagesEndeRunden(arbeitstag, reportObjekt);
            TagBeendenService.LOGGER.info("tagesEndeRunden");
        }

        TagBeendenService.LOGGER.info("Pausen generieren");
        pausenGenerieren(reportObjekt);
        TagBeendenService.LOGGER.info("Pausen generiert");
        ReportObjekt objekt = reportObjektDAO.merge(reportObjekt);

        TagBeendenService.LOGGER.info("merged startzeit = " + reportObjektDAO.findById(objekt.getId()).getTagesStart());
        TagBeendenService.LOGGER.info("merged endzeit = " + reportObjektDAO.findById(objekt.getId()).getTagesEnde());
    }

    private void pausenGenerieren(ReportObjekt reportObjekt) {
        String differenz;
        try {
            differenz = UhrzeitUtil.getDifferenz(reportObjekt.getTagesEnde(), reportObjekt.getTagesStart());
            GregorianCalendar calendar = new GregorianCalendar(Locale.GERMANY);
            calendar.set(Calendar.HOUR_OF_DAY, calendar.get(Calendar.HOUR_OF_DAY) + 4);

            String zeit = UhrzeitUtil.uhrzeitVariieren(UhrzeitUtil.format(calendar));
            Pause pause = new Pause(reportObjekt.getArbeitstag(), zeit);

            if (UhrzeitUtil.compare(differenz, "9:00") == 1) {
                calendar.set(Calendar.MINUTE, calendar.get(Calendar.MINUTE) + 90);
                pause.setEnde(UhrzeitUtil.format(calendar));

            } else if (UhrzeitUtil.compare(differenz, "6:00") == 1) {
                calendar.set(Calendar.MINUTE, calendar.get(Calendar.MINUTE) + 45);
                pause.setEnde(UhrzeitUtil.format(calendar));
            }

            reportObjekt.setPausen(pause);
            reportObjektDAO.persist(reportObjekt);
        } catch (ParseException pe) {
            TagBeendenService.LOGGER.error("Pausen generieren fehlgeschlagen: ", pe);
        }
    }

    private void tagesEndeRunden(Arbeitstag arbeitstag, ReportObjekt reportObjekt) {
        if (null != arbeitstag.getTagesEnde()) {
            int vergleich = UhrzeitUtil.compare(arbeitstag.getTagesEnde(), "18:30");

            if (vergleich < 0) {
                reportObjekt.setTagesEnde("18:00");

            } else {
                reportObjekt.setTagesEnde("19:00");
            }
        } else {
            throw new IllegalStateException("arbeitstag.getTagesEnde() war null"); //$NON-NLS-1$
        }
    }

    public void reportObjectBeenden(ReportObjekt reportObjekt, String ende) {
        if (UhrzeitUtil.uhrzeitValidieren(ende)) {
            reportObjekt.setTagesEnde(ende);
            reportObjektDAO.merge(reportObjekt);

        } else {
            throw new IllegalArgumentException(ende);
        }
    }
}
