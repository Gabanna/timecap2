package de.rgse.timecap.service;

public class ServiceProvider {

    private static PausenService pausenService;

    private static TagAnalyseService tagAnalyseService;

    private static TagBeendenService tagBeendenService;

    private static TagBeginnenService tagBeginnenService;

    public static PausenService getPausenServiceInstance() {
        if (null == ServiceProvider.pausenService) {
            ServiceProvider.pausenService = new PausenService();
        }
        return ServiceProvider.pausenService;
    }

    public static TagAnalyseService getTagAnalyseServiceInstance() {
        if (null == ServiceProvider.tagAnalyseService) {
            ServiceProvider.tagAnalyseService = new TagAnalyseService();
        }
        return ServiceProvider.tagAnalyseService;
    }

    public static TagBeendenService getTagBeendenServiceInstance() {
        if (null == ServiceProvider.tagBeendenService) {
            ServiceProvider.tagBeendenService = new TagBeendenService();
        }
        return ServiceProvider.tagBeendenService;
    }

    public static TagBeginnenService getTagBeginnenService() {
        if (null == ServiceProvider.tagBeginnenService) {
            ServiceProvider.tagBeginnenService = new TagBeginnenService();
        }
        return ServiceProvider.tagBeginnenService;
    }
}
